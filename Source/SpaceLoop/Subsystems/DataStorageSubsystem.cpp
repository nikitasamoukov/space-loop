#include "DataStorageSubsystem.h"
#include <SpaceLoop/DataAssets/SpaceLoopDeveloperSettings.h>
#include "RegistrySubsystem.h"
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>

#include "SpaceLoop/World/Helpers/ObjectPacked.h"

bool ScaleUpEntity(FFactoryComponents& FactoryComponents, FEnttHandle Handle)
{
	auto CompModuleBase = Handle.try_get<FCompModuleBase>();
	if (!CompModuleBase)
	{
		LOG_CRIT();
		return false;
	}
	if (CompModuleBase->Size == EModuleSize::None || CompModuleBase->Size == EModuleSize::XXL)
	{
		return false;
	}

	for (auto&& Pair : Handle.storage())
	{
		auto Cell = FactoryComponents.GetTypeCell(Pair.first);
		if (Cell->FuncModuleScaleUp)
		{
			Cell->FuncModuleScaleUp(Handle);
		}
		else
			LOG_CRIT();
	}

	return true;
}

void UDataStorageSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	SpaceLoopDataAsset = GetDefault<USpaceLoopDeveloperSettings>()->SpaceLoopDataAsset.LoadSynchronous();
}

void UDataStorageSubsystem::Deinitialize() {}

void UDataStorageSubsystem::BeginPlay()
{
	Objects.Empty();

	if (!SpaceLoopDataAsset || !SpaceLoopDataAsset->ObjectsArrayDataAsset)
		return;

	auto RegistrySubsystem = GetGameInstance()->GetSubsystem<URegistrySubsystem>();

	auto ObjectsArrayDataAsset = SpaceLoopDataAsset->ObjectsArrayDataAsset;

	for (auto Object : ObjectsArrayDataAsset->Objects)
	{
		if (!Object)
			continue;

		FEnttHandle HandleBaseData = RegistrySubsystem->CreateHandle();
		FObjectPacked::Unpack(this, Object->Components, HandleBaseData);

		if (auto CompModuleBase = HandleBaseData.try_get<FCompModuleBase>())
		{
			if (CompModuleBase->Type == EShipModuleType::None)
			{
				LOG("CompModuleBase->Type == EShipModuleType::None :" + Object->GetName());
			}
		}
		if (auto CompModuleBase = HandleBaseData.try_get<FCompModuleBase>())
		{
			if (CompModuleBase->ActivationType == EShipModuleActivationType::None)
				LOG("CompModuleBase->ActivationType == EShipModuleActivationType::None :" + Object->GetName());
			if (CompModuleBase->ActivationType == EShipModuleActivationType::AlwaysActive && CompModuleBase->ActivationTime != 0)
				LOG("CompModuleBase->ActivationTime != 0 :" + Object->GetName());
		}

		if (HandleBaseData.all_of<FCompModuleBase>())
		{
			for (int i = 0; i < 100; i++)
			{
				auto Handle = CopyEntity(HandleBaseData);
				auto& CompModuleBase = Handle.get<FCompModuleBase>();

				FString ModuleId = Object->Id.ToString();

				switch (CompModuleBase.Size)
				{
					case EModuleSize::XS:
						ModuleId += "_XS";
						break;
					case EModuleSize::S:
						ModuleId += "_S";
						break;
					case EModuleSize::M:
						ModuleId += "_M";
						break;
					case EModuleSize::L:
						ModuleId += "_L";
						break;
					case EModuleSize::XL:
						ModuleId += "_XL";
						break;
					case EModuleSize::XXL:
						ModuleId += "_XXL";
						break;
					default:
						LOG_ERROR();
				}

				FEnttHandle ObjectPacked = CopyEntity(Handle);

				Objects.FindOrAdd(FName(ModuleId)) = ObjectPacked;

				bool ScaleRes = ScaleUpEntity(GFactoryComponents, HandleBaseData);
				if (!ScaleRes)
					break;
			}
		}
		else
		{
			LOG_CRIT();
		}
		HandleBaseData.destroy();
	}
}