#include "RegistrySubsystem.h"
#include <Engine/World.h>
#include <SpaceLoop/World/Systems/SystemsRunner.h>

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

void URegistrySubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	PreWorldActorTickHandle = FWorldDelegates::OnWorldPreActorTick.AddUObject(this, &URegistrySubsystem::TickPreActors);
}

void URegistrySubsystem::Deinitialize()
{
	FWorldDelegates::OnWorldPreActorTick.Remove(PreWorldActorTickHandle);
}

FEnttHandle URegistrySubsystem::CreateHandle()
{
	return FEnttHandle(Registry, Registry.create());
}

void URegistrySubsystem::TickPreActors(UWorld* World, ELevelTick TickType, float DeltaSeconds)
{
	if (World->WorldType != EWorldType::Type::Game && World->WorldType != EWorldType::Type::PIE)
		return;

	TRACE_CPUPROFILER_EVENT_SCOPE_FUNCTION()

	SystemsRun(World, Registry, DeltaSeconds);
}
TArray<ASpaceship*> URegistrySubsystem::GetAllSpaceships()
{
	TArray<ASpaceship*> Res;

	for (auto [e, CompParentShip] :
		Registry.view<FCompParentShip>().each())
	{
		Res.Add(CompParentShip.Spaceship.Get());
	}

	return Res;
}