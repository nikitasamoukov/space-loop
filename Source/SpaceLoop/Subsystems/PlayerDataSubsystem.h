#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include <SpaceLoop/Common/EnttHandle.h>
#include <SpaceLoop/Structs/ModuleSlot.h>
#include "PlayerDataSubsystem.generated.h"

class UGalaxy;
struct FGalaxyStar;

UCLASS()
class UPlayerDataSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	// Begin USubsystem
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	// End USubsystem

	UFUNCTION(BlueprintCallable)
	void StartNewGame();

	UFUNCTION(BlueprintCallable)
	void Clear();

	UFUNCTION(BlueprintCallable)
	void GiveAll();
	
	UFUNCTION(BlueprintCallable)
	void OnMoveToStar(FGuid StarGuid);
	
	UFUNCTION(BlueprintCallable)
	FGalaxyStar FindStar(FGuid Guid);

	UFUNCTION(BlueprintCallable)
	TArray<FShipModuleSlotWithIdx> GetSlotsIdx();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int InventorySize = 20;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FEnttHandle> Items;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGuid PlayerStarGuid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UGalaxy* Galaxy = nullptr;
};