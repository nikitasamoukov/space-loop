﻿#include "SpaceLoopWorldSubsystem.h"

#include "DataStorageSubsystem.h"
#include "SpaceLoop/DataAssets/FactionDataAsset.h"

void USpaceLoopWorldSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Collection.InitializeDependency<UDataStorageSubsystem>();

	BeginPlay();
}
void USpaceLoopWorldSubsystem::Deinitialize()
{
}
void USpaceLoopWorldSubsystem::BeginPlay()
{
	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();

	if (!DataStorageSubsystem->SpaceLoopDataAsset || !DataStorageSubsystem->SpaceLoopDataAsset->FactionsTableDataAsset)
	{
		LOG_ERROR();
		return;
	}
	auto FactionsData = DataStorageSubsystem->SpaceLoopDataAsset->FactionsTableDataAsset->Factions;

	for (auto& Pair : FactionsData)
	{
		if (Pair.Key == "")
		{
			LOG_ERROR();
			continue;
		}
		if (!Pair.Value)
		{
			LOG_ERROR();
			continue;
		}
		Factions.FindOrAdd(Pair.Key) = Pair.Value->Faction;
		Factions.FindOrAdd(Pair.Key).Id = Pair.Key;
	}
}