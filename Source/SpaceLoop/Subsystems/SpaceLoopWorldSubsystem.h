﻿#pragma once
#include "SpaceLoop/World/Common/Faction.h"
#include "SpaceLoopWorldSubsystem.generated.h"

UCLASS()
class USpaceLoopWorldSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	// Begin USubsystem
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	// End USubsystem

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, FFaction> Factions;
	
	void BeginPlay();

private:
	// All my variables
};