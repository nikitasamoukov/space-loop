#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include <SpaceLoop/Common/EnttHandle.h>
#include <SpaceLoop/World/Components/FactoryComponents.h>
#include "RegistrySubsystem.generated.h"

class ASpaceship;

UCLASS()
class URegistrySubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	// Begin USubsystem
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	// End USubsystem

	FEnttHandle CreateHandle();

	void TickPreActors(UWorld* World, ELevelTick TickType, float DeltaSeconds);

	UFUNCTION(BlueprintCallable)
	TArray<ASpaceship*> GetAllSpaceships();

	FRegistry Registry;

	FDelegateHandle PreWorldActorTickHandle;

private:
	// All my variables
};
