#include "PlayerDataSubsystem.h"
#include "RegistrySubsystem.h"
#include "Subsystems/SubsystemCollection.h"
#include "DataStorageSubsystem.h"
#include "SpaceLoop/World/Galaxy/Galaxy.h"

#include <SpaceLoop/World/Components/ComponentsModulesBase.h>
#include <SpaceLoop/World/Helpers/HelpersModules.h>

void UPlayerDataSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Galaxy = NewObject<UGalaxy>();
}

void UPlayerDataSubsystem::Deinitialize()
{
}
void UPlayerDataSubsystem::StartNewGame()
{
	Clear();

	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();

	Galaxy->Load(DataStorageSubsystem->SpaceLoopDataAsset->GalaxyPreGenerated);

	Items.SetNum(InventorySize);
	GiveAll();
}
void UPlayerDataSubsystem::Clear()
{
	Items.Reset();
	Galaxy->Clean();
}
void UPlayerDataSubsystem::GiveAll()
{
	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();

	for (auto& Pair : DataStorageSubsystem->Objects)
	{
		FEnttHandle Handle = CreateShipModule(this, Pair.Key, FGuid::NewGuid());

		if (Handle.all_of<FCompModuleBase>())
		{
			Items.Add(Handle);
		}
		else
		{
			LOG_CRIT();
			Handle.destroy();
		}
	}
}

void UPlayerDataSubsystem::OnMoveToStar(FGuid StarGuid)
{
	auto Star = Galaxy->FindStar(StarGuid);
	if (!Star)
	{
		LOG_ERROR();
		return;
	}

	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();

	Star->GenLevelInfo(DataStorageSubsystem->SpaceLoopDataAsset);

	PlayerStarGuid = StarGuid;
}

FGalaxyStar UPlayerDataSubsystem::FindStar(FGuid Guid)
{
	auto Star = Galaxy->FindStar(Guid);

	return Star ? *Star : FGalaxyStar();
}

TArray<FShipModuleSlotWithIdx> UPlayerDataSubsystem::GetSlotsIdx()
{
	TArray<FShipModuleSlotWithIdx> Res;

	for (int i = 0; i < Items.Num(); i++)
	{
		auto& Handle = Items[i];

		FShipModuleSlotWithIdx SlotIdx;
		SlotIdx.Handle = Handle;
		SlotIdx.SlotType = EShipModuleType::None;
		SlotIdx.Idx = i;
		Res.Add(SlotIdx);
	}

	return Res;
}