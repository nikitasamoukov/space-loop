#pragma once
#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include <SpaceLoop/Common/EnttHandle.h>
#include "SpaceLoop/DataAssets/SpaceLoopDataAsset.h"
#include "DataStorageSubsystem.generated.h"

UCLASS()
class UDataStorageSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	// Begin USubsystem
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	// End USubsystem

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USpaceLoopDataAsset* SpaceLoopDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, FEnttHandle> Objects;

	void BeginPlay();

private:
	// All my variables
};
