#include "Pch.h"

DEFINE_LOG_CATEGORY(LogSpaceLoop);

void LOG_IMPL(const char* File, int Line, const char* Function, FString const& Text)
{
	UE_LOG(LogSpaceLoop, Warning, TEXT("%s (file:%s line:%i func:%s"), *Text, *FString(File), Line, *FString(Function));
}
void LOG_IMPLFatal(const char* File, int Line, const char* Function, FString const& Text)
{
	UE_LOG(LogSpaceLoop, Error, TEXT("%s (file:%s line:%i func:%s"), *Text, *FString(File), Line, *FString(Function));
}
