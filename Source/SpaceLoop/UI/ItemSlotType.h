#pragma once

#include "ItemSlotType.generated.h"

UENUM(BlueprintType)
enum class EItemSlotType : uint8
{
	None,
	ShipModules,
	Inventory,
	DragNDrop,
};

USTRUCT(BlueprintType)
struct FItemSlotIdx
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EItemSlotType SlotType = EItemSlotType::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Idx = 0;

	bool friend operator==(FItemSlotIdx const& A, FItemSlotIdx const& B) = default;
};