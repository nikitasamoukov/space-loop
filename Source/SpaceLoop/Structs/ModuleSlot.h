#pragma once

#include "CoreMinimal.h"
#include <SpaceLoop/Common/EnttHandle.h>

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"
#include "ModuleSlot.generated.h"

USTRUCT(BlueprintType)
struct FShipModuleSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EShipModuleType SlotType = EShipModuleType::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEnttHandle Handle;
};

USTRUCT(BlueprintType)
struct FShipModuleSlotWithIdx : public FShipModuleSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Idx = -1;
};

UCLASS()
class UFShipModuleSlot : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool CanFitInSlot(FShipModuleSlot const& ShipModuleSlot, FEnttHandle const& ModuleHandle);
};