#include "ModuleSlot.h"

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

bool UFShipModuleSlot::CanFitInSlot(FShipModuleSlot const& ShipModuleSlot, FEnttHandle const& ModuleHandle)
{
	if (!ModuleHandle.IsValid())
		return true;
	auto CompModuleBase = ModuleHandle.try_get<FCompModuleBase>();
	if (!CompModuleBase)
	{
		return true;
	}

	auto SlotType = ShipModuleSlot.SlotType;
	auto ModuleType = CompModuleBase->Type;

	switch (SlotType)
	{
		case EShipModuleType::None:
			return true;
	}
	
	return ModuleType == SlotType;

}
