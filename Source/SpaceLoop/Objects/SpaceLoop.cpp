#include "SpaceLoop.h"
#include "Modules/ModuleManager.h"
#include <ShaderCore.h>

void FSpaceLoopModule::StartupModule()
{
	auto Dir = FPaths::ProjectDir() / L"ShadersUsf";
	AddShaderSourceDirectoryMapping(L"/ShadersUsf", Dir);
}

void FSpaceLoopModule::ShutdownModule()
{
}

IMPLEMENT_PRIMARY_GAME_MODULE(FSpaceLoopModule, SpaceLoop, "SpaceLoop");
