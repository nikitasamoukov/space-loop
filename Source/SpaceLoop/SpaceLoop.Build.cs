// Copyright Epic Games, Inc. All Rights Reserved.

using EpicGames.Core;
using UnrealBuildTool;

public class SpaceLoop : ModuleRules
{
    public SpaceLoop(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_2;
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

        PrivateDependencyModuleNames.AddRange(new string[] { "StructUtils", "Niagara" });

        PrivateDependencyModuleNames.AddRange(new string[] { "RenderCore" });

        PCHUsage = PCHUsageMode.NoSharedPCHs;
        PrivatePCHHeaderFile = "PCH.h";
        CppStandard = CppStandardVersion.Cpp20;

        PrivateIncludePaths.Add("./../ThirdParty/include");
        PrivateIncludePaths.Add("./");
        
        
    }
}
