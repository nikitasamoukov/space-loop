#pragma once
#include "HelpersBpLib.generated.h"

UCLASS()
class UHelpersBpLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static double GetUmgDpi();
};