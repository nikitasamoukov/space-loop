﻿#include "ActorHelpers.h"

#include "GameFramework/Actor.h"

void SetLabel(AActor* Actor, FString const& Label)
{
	#if WITH_EDITOR
		Actor->SetActorLabel(Label);
	#endif
}