#pragma once
#include <utility>
#include <random>
#include "Rg128.generated.h"

USTRUCT(BlueprintType)
struct FRgSeed
{
	GENERATED_BODY()
	FGuid Seed;

	void Add(FString const& Addition);
	void Add(FRgSeed const& Addition);
	void Add(FGuid const& Addition);
	void Add(int32 const& Addition);
};

USTRUCT(BlueprintType)
struct FRg128
{
	GENERATED_BODY()
	FRg128(FRgSeed Seed = {});
	void SetSeed(FRgSeed Seed);
	FGuid GenGuid();
	double GenDouble(double Max = 1);
	int GenInt(int Min, int Max);
	int GenIndex(int ArrayNum);

private:
	std::array<std::mt19937_64, 2> GenMt64;
};