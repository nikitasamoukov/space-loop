#pragma once

#include "HelpersBpLib.h"

#include "Engine/Engine.h"
#include "Engine/GameViewportClient.h"
#include "Engine/UserInterfaceSettings.h"

double UHelpersBpLib::GetUmgDpi()
{
	FVector2D ViewportSize;
	GEngine->GameViewport->GetViewportSize(ViewportSize);

	int32 X = FMath::FloorToInt(ViewportSize.X);
	int32 Y = FMath::FloorToInt(ViewportSize.Y);

	return GetDefault<UUserInterfaceSettings>(UUserInterfaceSettings::StaticClass())->GetDPIScaleBasedOnSize(FIntPoint(X, Y));
}