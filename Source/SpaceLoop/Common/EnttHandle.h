#pragma once
#include "Entity.h"
#include "Registry.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EnttHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttHandle
#if CPP
	: public entt::basic_handle<FRegistry>
#endif
{
	GENERATED_BODY()
public:
	FEnttHandle() {}
	FEnttHandle(FRegistry& Reg, FEntity Entity)
		: entt::basic_handle<FRegistry>(Reg, Entity) {}
	~FEnttHandle() {}

	bool IsValid() const;

	template <typename... Component>
	[[nodiscard]] decltype(auto) has() const
	{
		return all_of<Component...>();
	}

	bool friend operator==(FEnttHandle const& A, FEnttHandle const& B);
};

template <>
struct TStructOpsTypeTraits<FEnttHandle> : public TStructOpsTypeTraitsBase2<FEnttHandle>
{
	enum
	{
		WithIdenticalViaEquality = true,
	};
};

UCLASS()
class UEnttHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttHandle const& Handle) { return Handle.IsValid(); }
};

FEnttHandle CopyEntity(FEnttHandle Base);
void CopyEntity(FEnttHandle TemplateHandle, FEnttHandle EntityHandle);
