#pragma once
#include "Entity.generated.h"

USTRUCT(BlueprintType)
struct FEntity
{
	GENERATED_BODY()
public:
	using entity_type = entt::entt_traits<std::uint64_t>::entity_type;

	FEntity(entity_type Value = entt::null)
		: Val(Value)
	{}

	FEntity(const FEntity& Other)
		: Val(Other.Val)
	{}

	operator entity_type() const
	{
		return Val;
	}

protected:
	entity_type Val;
};

template<>
struct entt::entt_traits<FEntity> : entt::entt_traits<std::uint64_t> {};

template<>
struct TStructOpsTypeTraits<FEntity> : public TStructOpsTypeTraitsBase2<FEntity>
{
	enum
	{
		WithIdenticalViaEquality = true,
	};
};