
#include "EnttHandle.h"
#include <SpaceLoop/World/Components/FactoryComponents.h>

bool FEnttHandle::IsValid() const
{
	return !!(*this);
}

bool operator==(FEnttHandle const& A, FEnttHandle const& B)
{
	// OG_LOG("E1: " + FString::FromInt((int)A.Entity) + ",E2: " + FString::FromInt((int)B.Entity) + " Compare res:" + FString::FromInt(A.Entity == B.Entity));
	// OG_LOG("R1: " + FString::FromInt(*(int*)&A.Reg) + ",R2: " + FString::FromInt(*(int*)&B.Reg) + " Compare res:" + FString::FromInt(A.Reg == B.Reg));
	return A.entity() == B.entity() && A.registry() == B.registry();
}

FEnttHandle CopyEntity(FEnttHandle Base)
{
	FEnttHandle Res(*Base.registry(), Base.registry()->create());

	for (auto&& curr : Base.registry()->storage())
	{
		if (auto& storage = curr.second; storage.contains(Base.entity()))
		{
			storage.emplace(Res.entity(), storage.get(Base.entity()));
		}
	}

	return Res;
}

void CopyEntity(FEnttHandle TemplateHandle, FEnttHandle EntityHandle)
{
	// Not work.
	//for (auto [id, storage] : TemplateHandle.registry()->storage())
	//{
	//	if (auto* other = EntityHandle.registry()->storage(id); other && storage.contains(TemplateHandle.entity()))
	//	{
	//		other->emplace(EntityHandle.entity(), storage.get(TemplateHandle.entity()));
	//	}
	//}

	for (auto&& [IdType, Data] : TemplateHandle.storage())
	{
		auto Cell = GFactoryComponents.GetTypeCell(IdType);
		if (!Cell || !Cell->FuncCopy)
		{
			LOG("CopyEntity error on:" + FString::FromInt(IdType));
			continue;
		}
		Cell->FuncCopy(TemplateHandle, EntityHandle);
	}
}
