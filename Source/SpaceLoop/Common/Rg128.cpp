#include "Rg128.h"
#include "Sha256.h"

FRg128::FRg128(FRgSeed Seed)
{
	SetSeed(Seed);
}

void FRg128::SetSeed(FRgSeed Seed)
{
	GenMt64[0] = std::mt19937_64{ Seed.Seed.A + (uint64(Seed.Seed.B) << 32) };
	GenMt64[1] = std::mt19937_64{ Seed.Seed.C + (uint64(Seed.Seed.D) << 32) };
}

void SplitUint64(uint64 A, uint32& Lo, uint32& Hi)
{
	Lo = A;
	Hi = A >> 32;
}

FGuid FRg128::GenGuid()
{
	FGuid Res;
	SplitUint64(GenMt64[0](), Res.A, Res.B);
	SplitUint64(GenMt64[1](), Res.C, Res.D);

	return Res;
}
double FRg128::GenDouble(double Max)
{
	int GenId = std::uniform_int_distribution<int>(0, 1)(GenMt64[0]);
	double Res = std::uniform_real_distribution<double>(0, Max)(GenMt64[GenId]);
	return Res;
}
int FRg128::GenInt(int Min, int Max)
{
	int GenId = std::uniform_int_distribution<int>(0, 1)(GenMt64[0]);
	int Res = std::uniform_int_distribution<int>(Min, Max)(GenMt64[GenId]);
	return Res;
}
int FRg128::GenIndex(int ArrayNum)
{
	if (ArrayNum == 0)
		return -1;

	return GenInt(0, ArrayNum - 1);
}

void AddBinary(TArray<unsigned char>& Data, char Val)
{
	Data.Add(unsigned char(Val));
}
void AddBinary(TArray<unsigned char>& Data, uint32 Val)
{
	for (int i = 0; i < 4; i++)
		Data.Add(unsigned char(Val >> (i * 8)));
}
void AddBinary(TArray<unsigned char>& Data, int32 Val)
{
	for (int i = 0; i < 4; i++)
		Data.Add(unsigned char(uint32(Val) >> (i * 8)));
}
void AddBinary(TArray<unsigned char>& Data, FGuid const& Val)
{
	AddBinary(Data, Val.A);
	AddBinary(Data, Val.B);
	AddBinary(Data, Val.C);
	AddBinary(Data, Val.D);
}
void AddBinary(TArray<unsigned char>& Data, std::string const& Val)
{
	for (auto& Ch : Val)
		AddBinary(Data, Ch);
}
void AddBinary(TArray<unsigned char>& Data, FString const& Val)
{
	std::string Text = TCHAR_TO_UTF8(*Val);
	AddBinary(Data, Text);
}

void FromBinary(unsigned char* Data, uint32& Res)
{
	Res = 0;
	for (int i = 0; i < 4; i++)
		Res |= (uint32(Data[i]) << (i * 8));
}

FGuid ConvSha256ToGuid(FSHA256Signature Sig)
{
	FGuid Res;

	for (int i = 0; i < 16; i++)
		Sig.Signature[i] ^= Sig.Signature[i + 16];

	for (int i = 0; i < 4; i++)
		FromBinary(Sig.Signature + i * 4, Res[i]);

	return Res;
}

void FRgSeed::Add(FString const& Text)
{
	FSHA256Signature RetSig;
	TArray<unsigned char> Data;
	AddBinary(Data, Seed);
	AddBinary(Data, Text);
	// Function newer work. WTF UE?
	// FGenericPlatformMisc::GetSHA256Signature(Data.GetData(), Data.Num(), RetSig);
	GetSHA256SignatureWorkingVersion(Data.GetData(), Data.Num(), RetSig);
	Seed = ConvSha256ToGuid(RetSig);
}

void FRgSeed::Add(FRgSeed const& NewSeed)
{
	Add(NewSeed.Seed);
}

void FRgSeed::Add(FGuid const& NewGuid)
{
	FSHA256Signature RetSig;
	TArray<unsigned char> Data;
	AddBinary(Data, Seed);
	AddBinary(Data, NewGuid);
	GetSHA256SignatureWorkingVersion(Data.GetData(), Data.Num(), RetSig);
	Seed = ConvSha256ToGuid(RetSig);
}
void FRgSeed::Add(int32 const& Addition)
{
	FSHA256Signature RetSig;
	TArray<unsigned char> Data;
	AddBinary(Data, Seed);
	AddBinary(Data, Addition);
	GetSHA256SignatureWorkingVersion(Data.GetData(), Data.Num(), RetSig);
	Seed = ConvSha256ToGuid(RetSig);
}