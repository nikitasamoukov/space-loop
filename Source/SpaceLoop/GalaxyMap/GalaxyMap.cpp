﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "GalaxyMap.h"

#include "GalaxyMapBpRef.h"
#include "GalaxyMapStar.h"
#include "GalaxyMapStarsDrawer.h"
#include "GalaxyMapViewCamera.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "SpaceLoop/Common/ActorHelpers.h"
#include "SpaceLoop/Subsystems/DataStorageSubsystem.h"
#include "SpaceLoop/Subsystems/PlayerDataSubsystem.h"
#include "SpaceLoop/World/Galaxy/Galaxy.h"
#include "SpaceLoop/World/Galaxy/GalaxyStarsStorage.h"

// Sets default values
AGalaxyMap::AGalaxyMap()
{
	auto Comp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Comp;

	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGalaxyMap::BeginPlay()
{
	Super::BeginPlay();
	CreateChildActors();

	if (!StarsDrawerGalaxy || !ViewCamera)
	{
		LOG_ERROR();
		return;
	}

	UpdateStarsFull();
}
void AGalaxyMap::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	// RemovedFromWorld ???
	ClearView();
	ViewCamera->Destroy();
	ViewCamera = nullptr;
	StarsDrawerGalaxy->Destroy();
	StarsDrawerGalaxy = nullptr;
}

// Called every frame
void AGalaxyMap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!StarsDrawerGalaxy || !ViewCamera)
	{
		LOG_ERROR();
		return;
	}

	bool bShowMap = GetWorld()->GetFirstPlayerController()->GetPawn() == (APawn*)ViewCamera;

	StarsDrawerGalaxy->SetActorHiddenInGame(!bShowMap);

	UpdateStarsView();
}
void AGalaxyMap::OnOpenMap()
{
	SetViewToCurrentStar();

	// LoadSelectedStarToView

	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();
	auto StarsStorage = PlayerDataSubsystem->Galaxy->StarsStorage;

	if (SelectedStarGuid != FGuid())
	{
		auto Star = StarsStorage->FindStar(SelectedStarGuid);
		if (!StarsInView.Contains(SelectedStarGuid) && Star)
		{
			auto StarActor = CreateStarUiActor(*Star);

			StarsInView.Add(SelectedStarGuid, StarActor);
		}
	}
}

void AGalaxyMap::OnCloseMap()
{
	ClearView();
}
void AGalaxyMap::SetViewToCurrentStar()
{
	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();

	auto Star = PlayerDataSubsystem->Galaxy->FindStar(PlayerDataSubsystem->PlayerStarGuid);
	if (!Star)
	{
		LOG_ERROR();
		return;
	}
	ViewCamera->GetRootComponent()->SetRelativeLocation(Star->Pos);
	ViewCamera->CamYaw = 0;
	ViewCamera->CamPitch = -60;
}

void AGalaxyMap::CreateChildActors()
{
	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();
	auto GalaxyMapBpRef = DataStorageSubsystem->SpaceLoopDataAsset->GalaxyMapBpRef;

	if (GalaxyMapBpRef->StarsDrawer)
	{
		{
			FTransform Trs;
			StarsDrawerGalaxy = GetWorld()->SpawnActor<AGalaxyMapStarsDrawer>(GalaxyMapBpRef->StarsDrawer, Trs);
			SetLabel(StarsDrawerGalaxy, TEXT("StarsDrawerGalaxy"));
			StarsDrawerGalaxy->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		}
	}
	if (GalaxyMapBpRef->GalaxyMapViewCamera)
	{
		FTransform Trs;
		ViewCamera = GetWorld()->SpawnActor<AGalaxyMapViewCamera>(GalaxyMapBpRef->GalaxyMapViewCamera, Trs);
		ViewCamera->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void AGalaxyMap::UpdateStarsFull()
{
	TArray<FTransform> InstanceTransforms;

	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();
	for (auto& Star : PlayerDataSubsystem->Galaxy->StarsStorage->GetStars())
	{
		FTransform Trs = FTransform::Identity;

		Trs.SetLocation(Star.Pos);
		Trs.SetScale3D({ BBScale, BBScale, BBScale });
		InstanceTransforms.Add(Trs);
	}

	StarsDrawerGalaxy->ComponentsIsmStars["Star"]->ClearInstances();
	StarsDrawerGalaxy->ComponentsIsmStars["Star"]->AddInstances(InstanceTransforms, false);
}

void AGalaxyMap::UpdateStarsView()
{
	UpdateView(ViewCamera->GetRootComponent()->GetRelativeLocation(), ViewCamera->ViewRange);
}

void AGalaxyMap::UpdateView(FVector const& Pos, double Range)
{
	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();

	auto StarsStorage = PlayerDataSubsystem->Galaxy->StarsStorage;

	auto StarsIndices = StarsStorage->GetStarsInRange(Pos, Range);
	auto& Stars = StarsStorage->GetStars();

	TMap<FGuid, AGalaxyMapStar*> StarsInViewNew;

	bool IsViewVisible = ViewCamera->CamDistance < ViewCamera->ViewRange * 3;

	if (IsViewVisible)
		for (auto& StarIdx : StarsIndices)
		{
			auto& StarData = Stars[StarIdx];

			auto StarActorRef = StarsInView.Find(StarData.Guid);
			if (!StarActorRef)
			{
				auto StarActor = CreateStarUiActor(StarData);

				StarsInViewNew.Add(StarData.Guid, StarActor);
			}
			else
			{
				StarsInViewNew.Add(StarData.Guid, *StarActorRef);
			}
		}

	{
		TMap<FGuid, AGalaxyMapStar*> StarsRemoved;
		for (auto& Pair : StarsInView)
		{
			if (Pair.Value->bPlayerPos)
				continue;
			if (Pair.Value->bSelected)
				continue;
			if (!StarsInViewNew.Contains(Pair.Key))
			{
				StarsRemoved.Add(Pair);
			}
		}

		for (auto& Pair : StarsRemoved)
		{
			Pair.Value->Destroy();
		}

		for (auto& Pair : StarsRemoved)
		{
			if (StarsInView.Contains(Pair.Key))
				StarsInView.Remove(Pair.Key);
		}
	}

	StarsInView.Append(StarsInViewNew);

	// StarsDrawerView->ComponentStars->;

	// StarsDrawerGalaxy->SetActorHiddenInGame(IsViewVisible);
	// StarsDrawerView->SetActorHiddenInGame(!IsViewVisible);
}
AGalaxyMapStar* AGalaxyMap::CreateStarUiActor(FGalaxyStar const& StarData)
{
	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();
	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();
	auto GalaxyMapBpRef = DataStorageSubsystem->SpaceLoopDataAsset->GalaxyMapBpRef;

	FTransform Trs;
	Trs.SetLocation(StarData.Pos);
	auto StarActor = GetWorld()->SpawnActorDeferred<AGalaxyMapStar>(GalaxyMapBpRef->StarActor, Trs);

	if (!StarActor)
	{
		LOG_ERROR();
		return nullptr;
	}
	StarActor->GalaxyMap = this;
	StarActor->GalaxyStar = StarData;
	StarActor->bPlayerPos = StarActor->GalaxyStar.Guid == PlayerDataSubsystem->PlayerStarGuid;

	StarActor->FinishSpawning(Trs);

	SetLabel(StarActor, TEXT("Star_") + StarData.Guid.ToString());
	StarActor->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	return StarActor;
}
void AGalaxyMap::ClearView()
{
	TArray<AGalaxyMapStar*> Actors;
	for (auto& Pair : StarsInView)
	{
		Actors.Add(Pair.Value);
	}
	for (auto& Actor : Actors)
		Actor->Destroy();
	StarsInView.Reset();
}
void AGalaxyMap::OnSelectStar(FGuid StarGuid)
{
	SelectedStarGuid = StarGuid;
	UpdateStarSelect();
}
void AGalaxyMap::UpdateStarSelect()
{
	for (auto& Pair : StarsInView)
	{
		Pair.Value->Update();
	}
}