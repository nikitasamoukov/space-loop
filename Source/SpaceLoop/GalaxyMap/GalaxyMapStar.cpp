﻿#include "GalaxyMapStar.h"

#include "GalaxyMap.h"

AGalaxyMapStar::AGalaxyMapStar()
{
	auto Comp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Comp;
	PrimaryActorTick.bCanEverTick = true;
}

void AGalaxyMapStar::BeginPlay()
{
	Super::BeginPlay();

	Update();
}

void AGalaxyMapStar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
void AGalaxyMapStar::OnSelectStar()
{
	GalaxyMap->OnSelectStar(GalaxyStar.Guid);
}
void AGalaxyMapStar::Update()
{
	bSelected = GalaxyStar.Guid == GalaxyMap->SelectedStarGuid;

	UpdateUiLayout();
}