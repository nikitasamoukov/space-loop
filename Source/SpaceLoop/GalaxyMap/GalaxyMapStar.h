﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpaceLoop/World/Galaxy/GalaxyStar.h"
#include "GalaxyMapStar.generated.h"

class AGalaxyMap;
UCLASS()
class SPACELOOP_API AGalaxyMapStar : public AActor
{
	GENERATED_BODY()

public:
	AGalaxyMapStar();

protected:
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void OnSelectStar();
	
	void Update();
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateUiLayout();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	AGalaxyMap* GalaxyMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGalaxyStar GalaxyStar;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bSelected = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bPlayerPos = false;
};