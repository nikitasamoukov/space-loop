﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "GalaxyMapViewCamera.h"

#include "ChaosInterfaceWrapperCore.h"
#include "GalaxyMap.h"

AGalaxyMapViewCamera::AGalaxyMapViewCamera()
{
	auto Comp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Comp;
	
	PrimaryActorTick.bCanEverTick = true;
}

void AGalaxyMapViewCamera::BeginPlay()
{
	Super::BeginPlay();
}

void AGalaxyMapViewCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

