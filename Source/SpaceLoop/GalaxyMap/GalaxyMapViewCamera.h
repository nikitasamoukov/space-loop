﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GalaxyMapViewCamera.generated.h"

class AGalaxyMap;
UCLASS()
class SPACELOOP_API AGalaxyMapViewCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGalaxyMapViewCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	AGalaxyMap* GalaxyMap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = StaticParam)
	double ViewRange = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = StaticParam)
	double CamYaw = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = StaticParam)
	double CamPitch = -45;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = StaticParam)
	double CamDistance = 200;
};