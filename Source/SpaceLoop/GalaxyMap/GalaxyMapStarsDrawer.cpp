﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "GalaxyMapStarsDrawer.h"

#include "Components/HierarchicalInstancedStaticMeshComponent.h"

// Sets default values
AGalaxyMapStarsDrawer::AGalaxyMapStarsDrawer()
{
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ISMStars"));
	RootComponent = SceneComponent;
}

// Called when the game starts or when spawned
void AGalaxyMapStarsDrawer::BeginPlay()
{
	Super::BeginPlay();

	for (auto& Pair : StarMeshes)
	{
		auto IsmComp = NewObject<UInstancedStaticMeshComponent>(this);
		IsmComp->RegisterComponent();
		IsmComp->SetFlags(RF_Transient);
		IsmComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		IsmComp->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
		AddInstanceComponent(IsmComp);
		
		IsmComp->SetStaticMesh(Pair.Value);
		ComponentsIsmStars.Add(Pair.Key, IsmComp);
	}
}