﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "UObject/Object.h"
#include "GalaxyMapBpRef.generated.h"

class AGalaxyMapStar;
class AGalaxyMapViewCamera;
class AGalaxyMapStarsDrawer;

UCLASS()
class SPACELOOP_API UGalaxyMapBpRefDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AGalaxyMapStarsDrawer> StarsDrawer;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AGalaxyMapViewCamera> GalaxyMapViewCamera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AGalaxyMapStar> StarActor;
};
