﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GalaxyMapStarsDrawer.generated.h"

UCLASS()
class SPACELOOP_API AGalaxyMapStarsDrawer : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGalaxyMapStarsDrawer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, UStaticMesh*> StarMeshes;

	// Dont try show this in editor unless you have 500 GB ram
	UPROPERTY()
	TMap<FName, UInstancedStaticMeshComponent*> ComponentsIsmStars;
};