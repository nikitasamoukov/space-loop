﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GalaxyMapViewCamera.h"
#include "GameFramework/Actor.h"
#include "GalaxyMap.generated.h"

struct FGalaxyStar;
class AGalaxyMapStar;
class AGalaxyMapStarsDrawer;
UCLASS()
class SPACELOOP_API AGalaxyMap : public AActor
{
	GENERATED_BODY()

public:
	AGalaxyMap();
	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable)
	void OnOpenMap();
	UFUNCTION(BlueprintCallable)
	void OnCloseMap();
	UFUNCTION(BlueprintCallable)
	void SetViewToCurrentStar();

	void CreateChildActors();
	void UpdateStarsFull();
	void UpdateStarsView();
	void UpdateView(FVector const& Pos, double Range);
	AGalaxyMapStar* CreateStarUiActor(FGalaxyStar const& StarData);

	UFUNCTION(BlueprintCallable)
	void ClearView();
	void OnSelectStar(FGuid StarGuid);
	void UpdateStarSelect();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	AGalaxyMapStarsDrawer* StarsDrawerGalaxy;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FGuid, AGalaxyMapStar*> StarsInView;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	AGalaxyMapViewCamera* ViewCamera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double BBScale = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = StaticParam)
	FGuid SelectedStarGuid;
};