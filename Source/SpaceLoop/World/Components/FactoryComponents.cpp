#include "FactoryComponents.h"

#include "ComponentsModulesWarpDrive.h"
#include "SpaceLoop/World/Components/ComponentsModulesBase.h"
#include "SpaceLoop/World/Components/ComponentsModulesEnergyGen.h"
#include "SpaceLoop/World/Components/ComponentsModulesShields.h"
#include "SpaceLoop/World/Components/ComponentsModulesWeapons.h"
#include "SpaceLoop/World/Components/ComponentsModulesThrusters.h"
#include "SpaceLoop/World/Components/ComponentsModulesArmors.h"

FFactoryComponents GFactoryComponents;

template <typename C>
concept CHasDescGen = requires(C const& Inst) {
						  {
							  Inst.DescGen()
							  } -> std::convertible_to<FText>;
					  };

template <typename C>
concept CHasScaleUp = requires(C Comp) {
						  {
							  Comp.ScaleUp()
							  } -> std::convertible_to<void>;
					  };

template <typename C>
concept CHasRefill = requires(C Comp, FEnttHandle H) {
						 {
							 C::Refill(H)
							 } -> std::convertible_to<void>;
					 };

template <typename C>
void RegComp(FFactoryComponents& Factory, FString const& Name)
{
	FFactoryComponentsCell Cell;

	Cell.Name = Name;

	{
		// FuncHaveComp
		Cell.FuncHaveComp = [](FRegistry const& Reg, FEntity Entity) -> bool {
			return Reg.all_of<C>(Entity);
		};
	}

	{
		// FuncPack
		Cell.FuncPack = [](FEnttHandle EnttHandle, FInstancedStruct& PackedComp) {
			auto Comp = EnttHandle.try_get<C>();
			if (!Comp)
			{
				LOG_CRIT();
				return;
			}
			PackedComp = FInstancedStruct::Make<C>(*Comp);
		};
	}

	{
		// FuncUnpack
		Cell.FuncUnpack = [](FEnttHandle EnttHandle, FInstancedStruct PackedComp) {
			auto Typed = PackedComp.GetPtr<C>();
			if (!Typed)
			{
				LOG_CRIT();
				return;
			}
			EnttHandle.emplace<C>(*Typed);
		};
	}

	{
		// FuncCopy
		Cell.FuncCopy = [](FEnttHandle BreedHandle, FEnttHandle EntityHandle) {
			if constexpr (!std::is_empty_v<C>)
			{
				EntityHandle.emplace_or_replace<C>(BreedHandle.get<C>());
			}
			else
			{
				EntityHandle.emplace<C>();
			}
		};
	}

	{
		// FuncModuleDesc
		if constexpr (CHasDescGen<C>)
		{
			Cell.FuncModuleDesc = [](FEnttHandle EnttHandle) -> FText {
				auto Comp = EnttHandle.try_get<C>();
				if (Comp)
				{
					return Comp->DescGen();
				}
				return {};
			};
		}
	}

	{
		// FuncModuleScaleUp
		if constexpr (CHasScaleUp<C>)
		{
			Cell.FuncModuleScaleUp = [](FEnttHandle EnttHandle) {
				auto Comp = EnttHandle.try_get<C>();
				if (Comp)
				{
					Comp->ScaleUp();
				}
			};
		}
	}

	{
		// FuncModuleRefill
		if constexpr (CHasRefill<C>)
		{
			Cell.FuncModuleRefill = [](FEnttHandle EnttHandle) {
				C::Refill(EnttHandle);
			};
		}
	}

	Factory.AddType(entt::type_id<C>(), MoveTemp(Cell));
}

#define REG_COMPONENT(C) RegComp<C>(*this, #C)

static_assert(CHasDescGen<FCompModuleBase>);

FFactoryComponents::FFactoryComponents()
{
	REG_COMPONENT(FCompParentShip);
	REG_COMPONENT(FCompModuleBase);
	REG_COMPONENT(FCompSignature);
	REG_COMPONENT(FCompDescSignatureConstant);
	REG_COMPONENT(FCompEnergyConsume);
	REG_COMPONENT(FCompDescEnergyConsumeConst);

	REG_COMPONENT(FCompEnergyGen);
	REG_COMPONENT(FCompDescEnergyGenFuel);
	REG_COMPONENT(FCompDescEnergyGenConst);
	REG_COMPONENT(FCompDescEnergyCap);

	REG_COMPONENT(FCompDescShieldBase);
	REG_COMPONENT(FCompDescShieldNormal);
	REG_COMPONENT(FCompShieldNormal);
	REG_COMPONENT(FCompShipDescModuleShieldCloak);
	REG_COMPONENT(FCompDescShieldDecreaser);
	REG_COMPONENT(FCompDescShieldLayered);
	REG_COMPONENT(FCompShieldLayered);

	REG_COMPONENT(FCompDescThruster);

	REG_COMPONENT(FCompWeaponBase);
	REG_COMPONENT(FCompDescWeaponLaser);
	REG_COMPONENT(FCompDescWeaponCannon);
	REG_COMPONENT(FCompWeaponCannon);

	REG_COMPONENT(FCompDescArmorNormal);
	REG_COMPONENT(FCompArmorNormal);

	REG_COMPONENT(FCompDescWarpDrive);
}

#undef REG_COMPONENT

void FFactoryComponents::AddType(entt::type_info Ti, FFactoryComponentsCell&& CInfo)
{
	TypesMap.Add(Ti.hash(), CInfo);
	NameToTypeInfo.Add(*CInfo.Name, Ti);
}

FFactoryComponentsCell const* FFactoryComponents::GetTypeCell(entt::id_type TiHash)
{
	if (!TypesMap.Contains(TiHash))
		return nullptr;
	return &TypesMap[TiHash];
}

FFactoryComponentsCell const* FFactoryComponents::GetTypeCell(entt::type_info Ti)
{
	return GetTypeCell(Ti.hash());
}

FFactoryComponentsCell const* FFactoryComponents::GetTypeCell(FName CompName)
{
	if (!NameToTypeInfo.Contains(CompName))
		return nullptr;
	return GetTypeCell(NameToTypeInfo[CompName]);
}
