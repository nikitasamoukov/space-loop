#include "ComponentsModulesWarpDrive.h"

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

void FCompDescWarpDrive::ScaleUp()
{
	Range *= 1.1;
}

FText FCompDescWarpDrive::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("Range"), Range);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Радиус прыжка: {Range})ZZ"),
		Args);

	return Text;
}