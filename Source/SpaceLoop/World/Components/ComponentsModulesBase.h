#pragma once

#include <SpaceLoop/World/Resourses/ResourceMaterial.h>
#include <Engine/Texture2D.h>

#include "ComponentsModulesBase.generated.h"

class ASpaceship;

// next size 2x bigger, 3x cost, ~2x better stats
UENUM(BlueprintType)
enum class EModuleSize : uint8
{
	None,
	XS,
	S,
	M,
	L,
	XL,
	XXL,
};

UENUM(BlueprintType)
enum class EShipModuleType : uint8
{
	None,
	Weapon,
	Thruster,
	WarpDrive,
	Support,
	Armor,
};

UENUM(BlueprintType)
enum class EShipModuleActivationType : uint8
{
	None,
	AlwaysActive,
	OnlyTime,
	OnFullEnergy,
};

UENUM(BlueprintType)
enum class EShipModuleCurrentState : uint8
{
	None,
	Disabled,
	WaitFullEnergy,
	ActivationInProgress,
	Active,
};

USTRUCT(BlueprintType)
struct FCompParentShip
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TWeakObjectPtr<ASpaceship> Spaceship = nullptr;
};

USTRUCT(BlueprintType)
struct FResCost
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<EResourceMaterial, double> Res;

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompModuleBase
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FName BaseModuleId;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Desc;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* Icon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EShipModuleType Type = EShipModuleType::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EModuleSize Size = EModuleSize::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResCost ResCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Mass{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Volume{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Cost{ 0 };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bEnabled = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ActivationTime{ 0 };

	// Not Desc
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double ActivationProgress{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EShipModuleActivationType ActivationType = EShipModuleActivationType::None;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FGuid Guid;

	EShipModuleCurrentState GetCurrentState() const;

	void ScaleUp();
	FText DescGen() const;
};

UCLASS()
class UFCompModuleBaseLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static EShipModuleCurrentState GetCurrentState(FCompModuleBase const& CompModuleBase) { return CompModuleBase.GetCurrentState(); }
};

USTRUCT(BlueprintType)
struct FCompSignature
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double Signature{ 0 };
	
	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescSignatureConstant
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double SignatureConst{ 0 };

	void ScaleUp();
	FText DescGen() const;
};

USTRUCT(BlueprintType)
struct FCompEnergyConsume
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double EnergyConsume{ 0 };
	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescEnergyConsumeConst
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double EnergyConsumeConst{ 0 };

	void ScaleUp();
	FText DescGen() const;
};
