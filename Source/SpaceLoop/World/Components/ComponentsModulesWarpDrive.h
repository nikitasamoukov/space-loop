#pragma once
#include "ComponentsModulesWarpDrive.generated.h"

USTRUCT(BlueprintType)
struct FCompDescWarpDrive
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Range{ 0 };
	
	void ScaleUp();
	FText DescGen() const;
};