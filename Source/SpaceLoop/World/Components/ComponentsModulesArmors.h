#pragma once
#include <SpaceLoop/Common/EnttHandle.h>
#include <Materials/MaterialInstance.h>
#include "ComponentsModulesArmors.generated.h"

USTRUCT(BlueprintType)
struct FCompArmorNormal
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double Armor{ 0 };
	
	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescArmorNormal
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ArmorMax{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ArmorRegen{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ArmorBlock{ 0 };

	void ScaleUp();
	FText DescGen() const;
	static void Refill(FEnttHandle EnttHandle);
};