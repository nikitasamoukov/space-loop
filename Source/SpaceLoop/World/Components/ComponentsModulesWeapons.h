#pragma once

#include "NiagaraSystem.h"
#include "ComponentsModulesWeapons.generated.h"

USTRUCT(BlueprintType)
struct FCompWeaponBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsShooting = false;

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescWeaponLaser
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double EnergyFire{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Dps{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Range{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UNiagaraSystem* LaserBeamSFX = nullptr;

	void ScaleUp();
	FText DescGen() const;
};

USTRUCT(BlueprintType)
struct FCompDescWeaponCannon
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double EnergyShot{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Dmg{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Speed{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double AttackSpeed{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Range{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AActor> BulletActor;

	void ScaleUp();
	FText DescGen() const;
};

USTRUCT(BlueprintType)
struct FCompWeaponCannon
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double Reload{ 0 };

	void ScaleUp();
};