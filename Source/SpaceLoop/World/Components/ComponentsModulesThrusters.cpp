#include "ComponentsModulesThrusters.h"

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

void FCompDescThruster::ScaleUp()
{
	Thrust *= 2.5;
}

FText FCompDescThruster::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("Thrust"), Thrust);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Тяга: {Thrust})ZZ"),
		Args);

	return Text;
}
