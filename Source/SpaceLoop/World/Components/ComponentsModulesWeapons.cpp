#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

#include "ComponentsModulesWeapons.h"

void FCompDescWeaponLaser::ScaleUp()
{
	EnergyFire *= 2;
	Dps *= 2;
	Range *= 1.1;
}

FText FCompDescWeaponLaser::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("EnergyFire"), EnergyFire);
	Args.Add(TEXT("Dps"), Dps);
	Args.Add(TEXT("Range"), Range);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Эн.(стрельба): {EnergyFire}/с
Урон: {Dps}/с
Дальность: {Range})ZZ"),
		Args);

	return Text;
}

void FCompWeaponBase::ScaleUp()
{
}

void FCompDescWeaponCannon::ScaleUp()
{
	EnergyShot *= 2;
	Dmg *= 2;
	Range *= 1.1;
}

FText FCompDescWeaponCannon::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("EnergyShot"), EnergyShot);
	Args.Add(TEXT("EnergyPerSecond"), EnergyShot * AttackSpeed);
	Args.Add(TEXT("Dmg"), Dmg);
	Args.Add(TEXT("Dps"), Dmg * AttackSpeed);
	Args.Add(TEXT("Speed"), Speed);
	Args.Add(TEXT("AttackSpeed"), AttackSpeed);
	Args.Add(TEXT("Range"), Range);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Эн.(на выстрел): {EnergyPerSecond}/с ({EnergyShot})
Урон: {Dps}/с ({Dmg})
Скорость: {Speed}
Скорость атаки: {AttackSpeed}/с
Дальность: {Range})ZZ"),
		Args);

	return Text;
}

void FCompWeaponCannon::ScaleUp()
{
}
