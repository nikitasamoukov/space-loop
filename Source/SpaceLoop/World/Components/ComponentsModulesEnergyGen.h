#pragma once

#include "../Resourses/ResourceMaterial.h"
#include "ComponentsModulesEnergyGen.generated.h"

USTRUCT(BlueprintType)
struct FCompEnergyGen
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double EnergyGen{ 0 };

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescEnergyGenFuel
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double EnergyGenAmount{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EResourceMaterial FuelType = EResourceMaterial::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double FuelPerDay{ 0 };
	
	void ScaleUp();
	FText DescGen() const;
};

USTRUCT(BlueprintType)
struct FCompDescEnergyGenConst
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double EnergyGenAmount{ 0 };
	
	void ScaleUp();
	FText DescGen() const;
};

USTRUCT(BlueprintType)
struct FCompDescEnergyCap
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double EnergyCap{ 0 };
	
	void ScaleUp();
	FText DescGen() const;
};
