#pragma once

#include <SpaceLoop/Common/EnttHandle.h>
#include "ComponentsModulesThrusters.generated.h"

USTRUCT(BlueprintType)
struct FCompDescThruster
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Thrust{ 0 };

	void ScaleUp();
	FText DescGen() const;
};
