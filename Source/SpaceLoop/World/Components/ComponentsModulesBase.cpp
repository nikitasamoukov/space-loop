﻿#include "ComponentsModulesBase.h"

EModuleSize NextSize(EModuleSize Size)
{
	switch (Size)
	{
		case EModuleSize::XS:
			return EModuleSize::S;
		case EModuleSize::S:
			return EModuleSize::M;
		case EModuleSize::M:
			return EModuleSize::L;
		case EModuleSize::L:
			return EModuleSize::XL;
		case EModuleSize::XL:
			return EModuleSize::XXL;
		case EModuleSize::XXL:
			return EModuleSize::None;
		default:
			LOG_ERROR();
	}
	return EModuleSize::None;
}

EShipModuleCurrentState FCompModuleBase::GetCurrentState() const
{
	switch (ActivationType)
	{
		case EShipModuleActivationType::AlwaysActive:
			return EShipModuleCurrentState::Active;
		case EShipModuleActivationType::OnlyTime:
			if (bEnabled)
			{
				if (ActivationProgress >= 1)
					return EShipModuleCurrentState::Active;
				return EShipModuleCurrentState::ActivationInProgress;
			}
			return EShipModuleCurrentState::Disabled;
		case EShipModuleActivationType::OnFullEnergy:
			if (bEnabled)
			{
				if (ActivationProgress >= 1)
					return EShipModuleCurrentState::Active;
				if (ActivationProgress > 0)
					return EShipModuleCurrentState::ActivationInProgress;
				if (ActivationProgress == 0)
					return EShipModuleCurrentState::WaitFullEnergy;
			}
			return EShipModuleCurrentState::Disabled;
		case EShipModuleActivationType::None:
			LOG_ERROR();
			return EShipModuleCurrentState::None;
	}
	LOG_ERROR();
	return EShipModuleCurrentState::None;
}

void FCompModuleBase::ScaleUp()
{
	Size = NextSize(Size);
	if (Size == EModuleSize::None)
	{
		LOG_CRIT();
	}
	Mass *= 2;
	Volume *= 2;
	for (auto& Pair : ResCost.Res)
	{
		Pair.Value *= 2;
	}
	ResCost.ScaleUp();
	Cost *= 3;
}

FText FCompModuleBase::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("Mass"), Mass);
	Args.Add(TEXT("Volume"), Volume);
	Args.Add(TEXT("Cost"), Cost);
	Args.Add(TEXT("ActivationTime"), ActivationTime);

	FText ActivationTypeText = FText::FromString("ZZZZZ");
	switch (ActivationType)
	{
		case EShipModuleActivationType::AlwaysActive:
			ActivationTypeText = FText::FromString(L"Всегда активно");
			break;
		case EShipModuleActivationType::OnlyTime:
			ActivationTypeText = FText::FromString(L"Только время");
			break;
		case EShipModuleActivationType::OnFullEnergy:
			ActivationTypeText = FText::FromString(L"При полной энергии");
			break;
		case EShipModuleActivationType::None:
			ActivationTypeText = FText::FromString(L"Нет");
			break;
		default:;
	}

	Args.Add(TEXT("ActivationType"), ActivationTypeText);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Масса: {Mass}
Объём: {Volume}
Стоимость: {Cost}
Условие включения: {ActivationType}
Время включения: {ActivationTime})ZZ"),
		Args);

	return Text;
}

void FCompDescSignatureConstant::ScaleUp()
{
	SignatureConst *= 2;
}

FText FCompDescSignatureConstant::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("SignatureConst"), SignatureConst);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Сигнатура: {SignatureConst})ZZ"),
		Args);

	return Text;
}

void FCompDescEnergyConsumeConst::ScaleUp()
{
	EnergyConsumeConst *= 2;
}

FText FCompDescEnergyConsumeConst::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("EnergyConsumeConst"), EnergyConsumeConst);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Энергопотребление: {EnergyConsumeConst}/с)ZZ"),
		Args);

	return Text;
}

void FCompSignature::ScaleUp()
{
}

void FCompEnergyConsume::ScaleUp()
{
}

void FResCost::ScaleUp()
{
	for (auto& Pair : Res)
	{
		Pair.Value *= 2;
	}
}
