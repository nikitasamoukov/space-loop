#include "ComponentsModulesArmors.h"

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

void FCompArmorNormal::ScaleUp()
{
}

void FCompDescArmorNormal::ScaleUp()
{
	ArmorMax *= 2;
	ArmorRegen *= 2;
	ArmorBlock *= 2;
}

FText FCompDescArmorNormal::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("ArmorMax"), ArmorMax);
	Args.Add(TEXT("ArmorRegen"), ArmorRegen);
	Args.Add(TEXT("ArmorBlock"), ArmorBlock);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Броня: {ArmorMax}
Регенерация: {ArmorRegen}/с
Блок урона: {ArmorBlock})ZZ"),
		Args);

	return Text;
}

void FCompDescArmorNormal::Refill(FEnttHandle EnttHandle)
{
	auto [CompDescArmorNormal, CompArmorNormal] = EnttHandle.try_get<FCompDescArmorNormal, FCompArmorNormal>();

	if (!CompDescArmorNormal || !CompArmorNormal)
	{
		LOG_ERROR();
		return;
	}
	CompArmorNormal->Armor = CompDescArmorNormal->ArmorMax;
}
