#include "ComponentsModulesShields.h"

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"

void FCompDescShieldNormal::ScaleUp()
{
	ShieldMax *= 2;
	ShieldRegen *= 2;
}

FText FCompDescShieldNormal::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("ShieldMax"), ShieldMax);
	Args.Add(TEXT("ShieldRegen"), ShieldRegen);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Щит: {ShieldMax}
Регенерация: {ShieldRegen}/с)ZZ"),
		Args);

	return Text;
}

void FCompDescShieldNormal::Refill(FEnttHandle EnttHandle)
{
	auto [CompDescShieldNormal, CompShieldNormal] = EnttHandle.try_get<FCompDescShieldNormal, FCompShieldNormal>();

	if (!CompDescShieldNormal || !CompShieldNormal)
	{
		LOG_ERROR();
		return;
	}
	CompShieldNormal->Shield = CompDescShieldNormal->ShieldMax;
}

void FCompDescShieldBase::ScaleUp()
{
}

void FCompShieldNormal::ScaleUp()
{
}

void FCompShipDescModuleShieldCloak::ScaleUp()
{
	SignatureMult *= 0.9;
}

void FCompDescShieldDecreaser::ScaleUp()
{
	DamageMult *= 0.9;
}

void FCompDescShieldLayered::ScaleUp()
{
	ShieldMax *= 2;
	ShieldRegen *= 2;
}

void FCompShieldLayered::ScaleUp()
{
}

void FCompDescShieldLayered::Refill(FEnttHandle EnttHandle)
{
	auto [CompDescShieldLayered, CompShieldLayered] = EnttHandle.try_get<FCompDescShieldLayered, FCompShieldLayered>();

	if (!CompDescShieldLayered || !CompShieldLayered)
	{
		LOG_ERROR();
		return;
	}
	CompShieldLayered->Shield = CompDescShieldLayered->ShieldMax;
	CompShieldLayered->ShieldRegenProgress = 0;
}
