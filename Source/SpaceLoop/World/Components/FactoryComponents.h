#pragma once


#include <SpaceLoop/Common/Registry.h>
#include <InstancedStruct.h>
#include <SpaceLoop/Common/EnttHandle.h>

struct FFactoryComponentsCell
{
	FString Name;

	using FFuncHaveComp = TFunction<bool(FRegistry const&, FEntity)>;
	using FFuncPack = TFunction<void(FEnttHandle EnttHandle, FInstancedStruct& PackedComp)>;
	using FFuncUnpack = TFunction<void(FEnttHandle EnttHandle, FInstancedStruct PackedComp)>;
	using FFuncCopy = TFunction<void(FEnttHandle TemplateHandle, FEnttHandle EntityHandle)>;
	using FFuncModuleDesc = TFunction<FText(FEnttHandle EnttHandle)>;
	using FFuncModuleScaleUp = TFunction<void(FEnttHandle EnttHandle)>;
	using FFuncModuleRefill = TFunction<void(FEnttHandle EnttHandle)>;

	FFuncHaveComp FuncHaveComp;
	FFuncPack FuncPack;
	FFuncUnpack FuncUnpack;
	FFuncCopy FuncCopy;
	FFuncModuleDesc FuncModuleDesc;
	FFuncModuleScaleUp FuncModuleScaleUp; // C.ScaleUp() call
	FFuncModuleRefill FuncModuleRefill;  // C.Refill() call
};

struct FFactoryComponents
{
	FFactoryComponents();

	void AddType(entt::type_info Ti, FFactoryComponentsCell&& CInfo);
	FFactoryComponentsCell const* GetTypeCell(entt::id_type TiHash);
	FFactoryComponentsCell const* GetTypeCell(entt::type_info Ti);
	FFactoryComponentsCell const* GetTypeCell(FName CompName);
	TMap<entt::id_type, FFactoryComponentsCell> const& GetAll() { return TypesMap; }

protected:
	TMap<entt::id_type, FFactoryComponentsCell> TypesMap;
	TMap<FName, entt::type_info> NameToTypeInfo;
};

extern FFactoryComponents GFactoryComponents;
