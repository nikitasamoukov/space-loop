#pragma once

#include "ComponentsShip.generated.h"

class ASpaceship;

USTRUCT(BlueprintType)
struct FShipHit
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Dmg = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bPeriodic = false;
};

USTRUCT(BlueprintType)
struct FCompShip
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ASpaceship* Spaceship = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FShipHit> Hits;
};
