#pragma once
#include <SpaceLoop/Common/EnttHandle.h>
#include <Materials/MaterialInstance.h>
#include "ComponentsModulesShields.generated.h"

USTRUCT(BlueprintType)
struct FCompDescShieldBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UMaterialInterface* Material = nullptr;

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescShieldNormal
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ShieldMax{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ShieldRegen{ 0 };

	void ScaleUp();
	FText DescGen() const;
	static void Refill(FEnttHandle EnttHandle);
};

USTRUCT(BlueprintType)
struct FCompShieldNormal
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double Shield{ 0 };

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompShipDescModuleShieldCloak
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double SignatureMult{ 0 };

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescShieldDecreaser
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double DamageMult{ 1 };

	void ScaleUp();
};

USTRUCT(BlueprintType)
struct FCompDescShieldLayered
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ShieldMax{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ShieldRegen{ 0 };

	void ScaleUp();
	static void Refill(FEnttHandle EnttHandle);
};

USTRUCT(BlueprintType)
struct FCompShieldLayered
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int Shield{ 0 };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double ShieldRegenProgress{ 0 };

	void ScaleUp();
};
