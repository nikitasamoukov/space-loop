#include "ComponentsModulesEnergyGen.h"

void FCompDescEnergyGenFuel::ScaleUp()
{
	EnergyGenAmount *= 2;
	FuelPerDay *= 2;
}

FText FCompDescEnergyGenFuel::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("EnergyGenAmount"), EnergyGenAmount);
	Args.Add(TEXT("FuelType"), UEnum::GetDisplayValueAsText(FuelType));
	Args.Add(TEXT("FuelPerDay"), FuelPerDay);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Энергия: +{EnergyGenAmount}/с
Топлива в день: {FuelPerDay}
Тип топлива: {FuelType})ZZ"),
		Args);

	return Text;
}


void FCompDescEnergyGenConst::ScaleUp()
{
	EnergyGenAmount *= 2;
}

FText FCompDescEnergyGenConst::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("EnergyGenAmount"), EnergyGenAmount);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Энергия: +{EnergyGenAmount}/с)ZZ"),
		Args);

	return Text;
}

void FCompDescEnergyCap::ScaleUp()
{
	EnergyCap *= 2;
}

FText FCompDescEnergyCap::DescGen() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("EnergyCap"), EnergyCap);
	auto Text = FText::Format(FText::FromString(LR"ZZ(Энергохранилище: {EnergyCap})ZZ"),
		Args);

	return Text;
}

void FCompEnergyGen::ScaleUp()
{
}
