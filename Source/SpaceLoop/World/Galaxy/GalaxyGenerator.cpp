﻿#include "GalaxyGenerator.h"

#include "GalaxyStar.h"
#include "SpaceLoop/Common/Rg128.h"
#include "SpaceLoop/DataAssets/GalaxyPreGeneratedDataAsset.h"
#include "kdtree/nanoflann.hpp"

struct FGalaxyGenSettings
{
	double GenGridSize = 50;

	double DensityChance = 0.3;
	// Density-stars count in 1x1x1
	double DensityMult = 0.02 / 100 / 100 / 100 / DensityChance;

	double WaySize = 11000;
	double WayWidth = 800;
	double WayAngle = 1.5 * 2 * PI;
	double WayEndSizeMult = 0.5;
	double WayDensity = 100;
	int WaysCount = 4;
	int PointsApproxCount = 200;

	double CentreSize = 2000;
	double CentreSizeVertical = 1000;
	double CentreDensity = 300;

	double LowPlaceSizeVertical = 1000;
	double LowPlaceSize = 12000;
	double LowPlaceDensity = 8;
};

constexpr FGalaxyGenSettings GGalaxyParam;

struct SegmentDistRes
{
	double Dist = 0;
	double Param = 0;
	int Type = 0; // 0-normal 1-p1 2-p2
};

SegmentDistRes SegmentDistance(FVector Pos, FVector2d S1, FVector2d S2)
{
	double x = Pos.X;
	double y = Pos.Y;
	double x1 = S1.X;
	double y1 = S1.Y;
	double x2 = S2.X;
	double y2 = S2.Y;

	double A = x - x1;
	double B = y - y1;
	double C = x2 - x1;
	double D = y2 - y1;

	double dot = A * C + B * D;
	double len_sq = C * C + D * D;
	double param = -1;
	if (len_sq != 0) // in case of 0 length line
		param = dot / len_sq;

	double xx, yy;

	SegmentDistRes Res;

	if (param < 0)
	{
		xx = x1;
		yy = y1;
		Res.Type = 1;
	}
	else if (param > 1)
	{
		xx = x2;
		yy = y2;
		Res.Type = 2;
	}
	else
	{
		xx = x1 + param * C;
		yy = y1 + param * D;
		Res.Type = 0;
	}
	Res.Param = param;

	double dx = x - xx;
	double dy = y - yy;
	Res.Dist = sqrt(dx * dx + dy * dy + Pos.Z * Pos.Z);

	return Res;
}

double ReScaleCos(double A)
{
	return 0.5 - cos(FMath::Clamp(A, 0, 1) * PI) / 2;
}

struct PointCloud
{
	using coord_t = double; //!< The type of each coordinate

	constexpr static inline int DIM = 2;

	std::vector<FVector2d> pts;

	// Must return the number of data points
	inline size_t kdtree_get_point_count() const { return pts.size(); }

	// Returns the dim'th component of the idx'th point in the class:
	// Since this is inlined and the "dim" argument is typically an immediate
	// value, the
	//  "if/else's" are actually solved at compile time.
	inline double kdtree_get_pt(const size_t idx, const size_t dim) const
	{
		if (dim == 0)
			return pts[idx].X;
		else
			return pts[idx].Y;
	}

	// Optional bounding-box computation: return false to default to a standard
	// bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned
	//   in "bb" so it can be avoided to redo it again. Look at bb.size() to
	//   find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox(BBOX& /* bb */) const
	{
		return false;
	}
};


struct FWaysKdTree
{
	// construct a kd-tree index:
	using my_kd_tree_t = nanoflann::KDTreeSingleIndexAdaptor<
		nanoflann::L2_Simple_Adaptor<double, PointCloud>,
		PointCloud, PointCloud::DIM /* dim */
		>;
	
	explicit FWaysKdTree(PointCloud const& CloudNew)
		: Cloud(CloudNew), Tree(PointCloud::DIM, Cloud, { 10 /* max leaf */ })
	{
	}

	int GetNNIdx(FVector2d const& Pos)
	{
		size_t num_results = 1;
		std::vector<uint32_t> ret_index(num_results);
		std::vector<double> out_dist_sqr(num_results);

		const double query_pt[2] = { Pos.X, Pos.Y };
		num_results = Tree.knnSearch(
			&query_pt[0], num_results, &ret_index[0], &out_dist_sqr[0]);

		return ret_index[0];
	}

	PointCloud Cloud;
	my_kd_tree_t Tree;
};

struct FGalaxyWays
{
	struct FWayInfo
	{
		double Perc = 0;
		double Dist = 0;
	};

	struct FWay
	{
		struct FResultNear
		{
			double Perc = 0;
			double Dist = 0;
		};
		struct FLine
		{
			FVector2D Pos1;
			double Perc1 = 0;
			FVector2D Pos2;
			double Perc2 = 0;

			FResultNear GetPerc(FVector Pos)
			{
				auto SegmentDistInfo = SegmentDistance(Pos, Pos1, Pos2);

				if (SegmentDistInfo.Type == 1)
				{
					return { Perc1, SegmentDistInfo.Dist };
				}
				if (SegmentDistInfo.Type == 2)
				{
					return { Perc2, SegmentDistInfo.Dist };
				}
				return { Perc1 * SegmentDistInfo.Param + Perc2 * (1 - SegmentDistInfo.Param), SegmentDistInfo.Dist };
			}
		};
		TArray<FVector2D> Points;

		TSharedPtr<FWaysKdTree> Kdtree;

		FWayInfo GetNearInfo(FVector Pos)
		{
			int Idx = Kdtree->GetNNIdx({ Pos.X, Pos.Y });
			Idx = FMath::Clamp(Idx, 1, Points.Num() - 2);

			FLine Line1(
				Points[Idx - 1], double(Idx - 1) / GGalaxyParam.PointsApproxCount,
				Points[Idx], double(Idx) / GGalaxyParam.PointsApproxCount);
			FLine Line2(
				Points[Idx + 1], double(Idx + 1) / GGalaxyParam.PointsApproxCount,
				Points[Idx], double(Idx) / GGalaxyParam.PointsApproxCount);

			auto Info1 = Line1.GetPerc(Pos);
			auto Info2 = Line2.GetPerc(Pos);

			FWayInfo Res;

			if (Info1.Dist < Info2.Dist)
			{
				Res.Dist = Info1.Dist;
				Res.Perc = Info1.Perc;
			}
			else
			{
				Res.Dist = Info2.Dist;
				Res.Perc = Info2.Perc;
			}
			return Res;
		}
	};

	void Init()
	{
		Ways.SetNum(0);
		Ways.SetNum(GGalaxyParam.WaysCount);
		for (int i = 0; i < GGalaxyParam.WaysCount; i++)
		{
			auto& Way = Ways[i];
			double StartAngle = i * PI * 2 / GGalaxyParam.WaysCount;

			for (int r = 0; r <= GGalaxyParam.PointsApproxCount; r++)
			{
				double Perc = r / (double)GGalaxyParam.PointsApproxCount;
				double AngleAdd = Perc * GGalaxyParam.WayAngle;
				double Dist = Perc * GGalaxyParam.WaySize;

				FVector2D Pos(cos(StartAngle + AngleAdd) * Dist, sin(StartAngle + AngleAdd) * Dist);
				Way.Points.Add(Pos);
			}
			PointCloud Cloud;
			Cloud.pts.resize(Way.Points.Num());
			for (int r = 0; r <= GGalaxyParam.PointsApproxCount; r++)
			{
				Cloud.pts[r][0] = Way.Points[r].X;
				Cloud.pts[r][1] = Way.Points[r].Y;
			}

			Way.Kdtree = MakeShared<FWaysKdTree>(Cloud);
		}
	}

	double GetWayDensity(FVector Pos)
	{
		double Res = 0;
		for (auto& Way : Ways)
		{
			auto Info = Way.GetNearInfo(Pos);
			double WidthMax = FMath::Lerp(GGalaxyParam.WayWidth, GGalaxyParam.WayEndSizeMult * GGalaxyParam.WayWidth, Info.Perc);
			double DensityMax = (1 - Info.Perc) * GGalaxyParam.WayDensity;

			double DistPerc = (WidthMax - Info.Dist) / WidthMax;
			if (DistPerc >= 0)
			{
				Res = FMath::Max(Res, ReScaleCos(DistPerc) * DensityMax);
			}
		}
		return Res;
	}

	TArray<FWay> Ways;
};

void FGalaxyGenerator::Generate()
{
	Stars.Reset();
	GenerateStars();
	ConvertToAsset();
}
void FGalaxyGenerator::GenerateStars()
{
	FGalaxyWays GalaxyWays;

	GalaxyWays.Init();

	auto DensityFunc = [&](FVector const& Pos) {
		double WayDens = GalaxyWays.GetWayDensity(Pos);

		auto PercCenter = 1 - FVector3d(Pos.X / GGalaxyParam.CentreSize, Pos.Y / GGalaxyParam.CentreSize, Pos.Z / GGalaxyParam.CentreSizeVertical).Length();
		auto CenterDens = ReScaleCos(FMath::Clamp(PercCenter, 0, 1)) * GGalaxyParam.CentreDensity;

		auto PercLow =
			1 - FVector(Pos.X / GGalaxyParam.LowPlaceSize, Pos.Y / GGalaxyParam.LowPlaceSize, Pos.Z / GGalaxyParam.LowPlaceSizeVertical).Length();
		auto LowDens = ReScaleCos(FMath::Clamp(PercLow, 0, 1)) * GGalaxyParam.LowPlaceDensity;

		double Density = FMath::Max3(WayDens, CenterDens, LowDens);
		return Density;
	};

	// for (int X = 0; X < GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize + 1; X++)
	// 	for (int Y = 0; Y < GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize + 1; Y++)
	// 		for (int Z = 0; Z < GGalaxyParam.LowPlaceSizeVertical / GGalaxyParam.GenGridSize + 1; Z++)

	for (int X = -GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize - 1; X < GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize + 1; X++)
	{
		for (int Y = -GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize - 1; Y < GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize + 1; Y++)
			for (int Z = -GGalaxyParam.LowPlaceSizeVertical / GGalaxyParam.GenGridSize - 1; Z < GGalaxyParam.LowPlaceSizeVertical / GGalaxyParam.GenGridSize + 1; Z++)
			{
				FIntVector PosInt(X, Y, Z);
				FVector Pos((X + 0.5) * GGalaxyParam.GenGridSize, (Y + 0.5) * GGalaxyParam.GenGridSize, (Z + 0.5) * GGalaxyParam.GenGridSize);

				double Dens = DensityFunc(Pos);
				Dens *= GGalaxyParam.DensityMult;

				FRgSeed SectorSeed(Seed);
				SectorSeed.Add(PosInt.X);
				SectorSeed.Add(PosInt.Y);
				SectorSeed.Add(PosInt.Z);

				GenerateStarsInCube(Dens, PosInt, SectorSeed, GGalaxyParam.GenGridSize);
			}
		LOG("X:" + FString::FromInt(X) + "/" + FString::FromInt(GGalaxyParam.LowPlaceSize / GGalaxyParam.GenGridSize));
	}
	LOG("Stars:" + FString::FromInt(Stars.Num()));
}
void FGalaxyGenerator::ConvertToAsset()
{
	GalaxyPreGenerated = NewObject<UGalaxyPreGeneratedDataAsset>();

	GalaxyPreGenerated->Seed = Seed;
	GalaxyPreGenerated->Stars = MoveTemp(Stars);
	GalaxyPreGenerated->StarsCount = GalaxyPreGenerated->Stars.Num();
}

void FGalaxyGenerator::GenerateStarsInCube(double Density, FIntVector PosInt, FRgSeed CubeSeed, double CubeSize)
{
	FRg128 Rg{ CubeSeed };

	double StarsToTry = Density * CubeSize * CubeSize * CubeSize;

	FVector PosMin(PosInt.X * CubeSize, PosInt.Y * CubeSize, PosInt.Z * CubeSize);

	int StarsCount = 0;

	for (int i = 0; i < FMath::Floor(StarsToTry); i++)
	{
		if (Rg.GenDouble() <= GGalaxyParam.DensityChance)
			StarsCount++;
	}
	if (Rg.GenDouble() <= GGalaxyParam.DensityChance * (StarsToTry - FMath::Floor(StarsToTry)))
		StarsCount++;

	for (int i = 0; i < StarsCount; i++)
	{
		FVector PosInSector(Rg.GenDouble(CubeSize), Rg.GenDouble(CubeSize), Rg.GenDouble(CubeSize));
		FVector Pos = PosMin + PosInSector;
		FGuid StarSeed = Rg.GenGuid();
		FGalaxyStar Star;
		Stars.Add({});
		Stars.Last().Guid = StarSeed;
		Stars.Last().Pos = Pos;
		Stars.Last().Generate();
	}
}