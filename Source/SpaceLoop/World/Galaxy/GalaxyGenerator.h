﻿#pragma once
#include "GalaxyStar.h"
#include "SpaceLoop/Common/Rg128.h"

class UGalaxyPreGeneratedDataAsset;

struct FGalaxyGenerator
{
	FGuid Seed;
	
	void Generate();
	void GenerateStars();
	void ConvertToAsset();
	TObjectPtr<UGalaxyPreGeneratedDataAsset> GalaxyPreGenerated;

	// Internal
	
	void GenerateStarsInCube(double Density, FIntVector PosInt, FRgSeed CubeSeed, double CubeSize);

	TArray<FGalaxyStar> Stars;
	
};
