﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Galaxy.h"

#include "GalaxyStarsStorage.h"
#include "SpaceLoop/DataAssets/GalaxyPreGeneratedDataAsset.h"

UGalaxy::UGalaxy()
{
	StarsStorage = NewObject<UGalaxyStarsStorage>();
}
void UGalaxy::Load(UGalaxyPreGeneratedDataAsset* GalaxyPreGenerated)
{
	if (!GalaxyPreGenerated)
	{
		LOG_ERROR();
		return;
	}
	StarsStorage = NewObject<UGalaxyStarsStorage>();
	StarsStorage->Init(GalaxyPreGenerated->Stars);
}
void UGalaxy::Clean()
{
}
FGalaxyStar* UGalaxy::FindStar(FGuid Guid)
{
	return StarsStorage->FindStar(Guid);
}