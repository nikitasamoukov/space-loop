﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpaceLoop/World/Common/Faction.h"
#include "UObject/Object.h"
#include "GalaxyStar.generated.h"

class USpaceLoopDataAsset;
class ASpaceship;

USTRUCT(BlueprintType)
struct SPACELOOP_API FStarStationInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FFaction Faction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Level = 1;
};

USTRUCT(BlueprintType)
struct SPACELOOP_API FStarShipInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Faction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<ASpaceship> ShipAsset;
};

USTRUCT(BlueprintType)
struct FStarLevelInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FStarStationInfo> Stations;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FStarShipInfo> Ships;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName LevelBaseName;
};

USTRUCT(BlueprintType)
struct FGalaxyStar
{
	GENERATED_BODY()

	void Generate();
	void GenLevelInfo(USpaceLoopDataAsset* SpaceLoopDataAsset);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector Pos = FVector::ZeroVector;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGuid Guid;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName FactionId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double FactionStrength = 0;

	UPROPERTY(EditAnywhere)
	TArray<FStarLevelInfo> LevelInfo;
};

UCLASS()
class UBflGalaxyStar : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool GetInfo(FGalaxyStar const& GalaxyStar, FStarLevelInfo& Info);
};