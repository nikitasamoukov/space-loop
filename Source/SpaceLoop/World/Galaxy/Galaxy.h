﻿#pragma once

#include "CoreMinimal.h"
#include "GalaxyStar.h"
#include "UObject/Object.h"
#include "Galaxy.generated.h"

class UGalaxyStarsStorage;
class UGalaxyPreGeneratedDataAsset;
UCLASS()
class SPACELOOP_API UGalaxySector : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGuid Seed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Density = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FGalaxyStar> Stars;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FIntVector PosInt;
};

UCLASS()
class SPACELOOP_API UGalaxy : public UObject
{
	GENERATED_BODY()
public:
	UGalaxy();

	FGuid Seed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UGalaxyStarsStorage* StarsStorage;

	void Load(UGalaxyPreGeneratedDataAsset* GalaxyPreGenerated);

	void Clean();

	FGalaxyStar* FindStar(FGuid Guid);
};