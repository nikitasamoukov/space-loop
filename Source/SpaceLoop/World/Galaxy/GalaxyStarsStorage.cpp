﻿#include "GalaxyStarsStorage.h"

#include <kdtree/nanoflann.hpp>

struct FPointCloud3D
{
	using coord_t = double; //!< The type of each coordinate

	constexpr static inline int DIM = 3;

	TArray<FVector> pts;

	// Must return the number of data points
	inline size_t kdtree_get_point_count() const { return pts.Num(); }

	// Returns the dim'th component of the idx'th point in the class:
	// Since this is inlined and the "dim" argument is typically an immediate
	// value, the
	//  "if/else's" are actually solved at compile time.
	inline double kdtree_get_pt(const size_t idx, const size_t dim) const
	{
		if (dim == 0)
			return pts[idx].X;
		else if (dim == 1)
			return pts[idx].Y;
		else
			return pts[idx].Z;
	}

	// Optional bounding-box computation: return false to default to a standard
	// bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned
	//   in "bb" so it can be avoided to redo it again. Look at bb.size() to
	//   find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox(BBOX& /* bb */) const
	{
		return false;
	}
};

struct FVectorKdTree
{ // construct a kd-tree index:
	using my_kd_tree_t = nanoflann::KDTreeSingleIndexAdaptor<
		nanoflann::L2_Simple_Adaptor<double, FPointCloud3D>,
		FPointCloud3D, FPointCloud3D::DIM /* dim */
		>;
	static FPointCloud3D Conv(TArray<FGalaxyStar> const& Points)
	{
		FPointCloud3D Cloud;
		Cloud.pts.SetNum(Points.Num());
		for (int r = 0; r < Points.Num(); r++)
		{
			Cloud.pts[r] = Points[r].Pos;
		}
		return Cloud;
	}

	explicit FVectorKdTree(TArray<FGalaxyStar> const& Points)
		: Cloud(Conv(Points)), Tree(FPointCloud3D::DIM, Cloud, { 10 /* max leaf */ })
	{
	}

	int GetNnIdx(FVector const& Pos)
	{
		size_t num_results = 1;
		std::vector<uint32_t> ret_index(num_results);
		std::vector<double> out_dist_sqr(num_results);

		const double query_pt[3] = { Pos.X, Pos.Y, Pos.Z };
		num_results = Tree.knnSearch(
			&query_pt[0], num_results, &ret_index[0], &out_dist_sqr[0]);

		return ret_index[0];
	}

	TArray<int> GetInRadius(FVector const& Pos, double Dist)
	{
		size_t num_results = 0;
		std::vector<nanoflann::ResultItem<uint32, double>> IndicesDists;
		const double query_pt[3] = { Pos.X, Pos.Y, Pos.Z };
		num_results = Tree.radiusSearch(&query_pt[0], Dist * Dist, IndicesDists);

		int Test = GetNnIdx(Pos);

		TArray<int> Res;

		for (auto& Item : IndicesDists)
		{
			Res.Add(Item.first);
		}

		return Res;
	}

	FPointCloud3D Cloud;
	my_kd_tree_t Tree;
};

struct UGalaxyStarsStorage::FImpl
{
	explicit FImpl(TArray<FGalaxyStar> const& NewStars)
		: Stars(NewStars)
		, Tree(Stars)
	{
	}

	TArray<FGalaxyStar> Stars;
	FVectorKdTree Tree;
};

UGalaxyStarsStorage::UGalaxyStarsStorage()
{
	Impl = MakeUnique<FImpl>(TArray<FGalaxyStar>());
}
UGalaxyStarsStorage::UGalaxyStarsStorage(FVTableHelper& Helper)
{
}
UGalaxyStarsStorage::~UGalaxyStarsStorage()
{
}

void UGalaxyStarsStorage::Init(TArray<FGalaxyStar> const& StarsNew)
{
	Impl = MakeUnique<FImpl>(StarsNew);
}

TArray<FGalaxyStar>& UGalaxyStarsStorage::GetStars()
{
	return Impl->Stars;
}
TArray<int> UGalaxyStarsStorage::GetStarsInRange(FVector const& Pos, double Range)
{
	return Impl->Tree.GetInRadius(Pos, Range);
}
FGalaxyStar* UGalaxyStarsStorage::FindStar(FGuid Guid)
{
	FGalaxyStar* Res = nullptr;

	Res = Impl->Stars.FindByPredicate([&](FGalaxyStar const& E) { return E.Guid == Guid; });

	return Res;
}