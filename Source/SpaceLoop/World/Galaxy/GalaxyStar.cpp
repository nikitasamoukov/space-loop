﻿#include "GalaxyStar.h"

#include "SpaceLoop/Common/Rg128.h"
#include "SpaceLoop/DataAssets/BaseLevelsDataAsset.h"
#include "SpaceLoop/DataAssets/SpaceLoopDataAsset.h"

void FGalaxyStar::Generate()
{
	auto Str = Guid.ToString();
	TCHAR Arr[] = { Str[0], Str[1], Str[2], '-', Str[3], Str[4], Str[5], 0 };
	Name = FString(Arr);
}

void FGalaxyStar::GenLevelInfo(USpaceLoopDataAsset* SpaceLoopDataAsset)
{
	if (LevelInfo.Num() > 0)
		return;

	FStarLevelInfo Info;

	if (!SpaceLoopDataAsset->BaseLevels || SpaceLoopDataAsset->BaseLevels->BaseLevels.Num() == 0)
	{
		LOG_ERROR();
		goto end_p;
	}
	// Info.
	{
		auto& BaseLevels = SpaceLoopDataAsset->BaseLevels->BaseLevels;

		FRg128 Rg = { FRgSeed(Guid) };

		TArray<FName> Keys;
		BaseLevels.GetKeys(Keys);

		int Idx = Rg.GenIndex(Keys.Num());
		Info.LevelBaseName = Keys[Idx];

		auto PFactionDa = SpaceLoopDataAsset->FactionsTableDataAsset->Factions.Find(FactionId);
		if (PFactionDa)
		{
			auto& Faction = (*PFactionDa)->Faction;

			auto& Ships = Faction.Ships;

			if (Ships.Num() > 0)
			{
				auto StrRelated = FactionStrength * (Rg.GenDouble(1) + 0.5);
				for (int i = 0; i < 20; i++)
				{
					auto& ShipInfo = Ships[Rg.GenIndex(Ships.Num())];
					if (ShipInfo.Strength < StrRelated)
					{
						StrRelated -= ShipInfo.Strength;
						FStarShipInfo StarShipInfo;
						StarShipInfo.Faction = FactionId;
						StarShipInfo.ShipAsset = ShipInfo.ShipBp;
						Info.Ships.Add(StarShipInfo);
					}
				}
			}
		}
	}
end_p:
	LevelInfo.Add(Info);
}

bool UBflGalaxyStar::GetInfo(FGalaxyStar const& GalaxyStar, FStarLevelInfo& Info)
{
	if (GalaxyStar.LevelInfo.Num() == 0)
	{
		return false;
	}
	Info = GalaxyStar.LevelInfo[0];

	return true;
}