﻿#pragma once

#include "GalaxyStar.h"
#include "GalaxyStarsStorage.generated.h"

UCLASS(BlueprintType)
class UGalaxyStarsStorage : public UObject
{
public:
	GENERATED_BODY()

	struct FImpl;
	
	UGalaxyStarsStorage();
	UGalaxyStarsStorage(FVTableHelper& Helper);
	virtual ~UGalaxyStarsStorage() override;


	void Init(TArray<FGalaxyStar> const& StarsNew);

	TArray<FGalaxyStar>& GetStars();
	TArray<int> GetStarsInRange(FVector const& Pos, double Range);

	FGalaxyStar* FindStar(FGuid Guid);
private:
	TUniquePtr<FImpl> Impl;
};