#pragma once
#include "../../Common/EnttHandle.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HelpersModules.generated.h"

UCLASS()
class UHelpersModulesLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static double GetWeaponBulletSpeed(FEnttHandle const& Handle);
};

FEnttHandle CreateShipModule(UObject* ContextObject, FName ModuleId, FGuid Guid);
void Refill(FEnttHandle Handle);
