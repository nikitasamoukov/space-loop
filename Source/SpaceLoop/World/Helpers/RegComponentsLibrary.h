// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "SpaceLoop/Common/EnttHandle.h"
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>
#include "RegComponentsLibrary.generated.h"

#define COMP_DEF(C)                                                                   \
	U##FUNCTION(BlueprintCallable) static bool HasComp##C(FEnttHandle Handle)         \
	{                                                                                 \
		if (!Handle)                                                                  \
			return false;                                                             \
		return Handle.has<C>();                                                       \
	}                                                                                 \
	U##FUNCTION(BlueprintCallable) static C GetComp##C(FEnttHandle Handle)            \
	{                                                                                 \
		return !!Handle && Handle.has<C>() ? Handle.get<C>() : C();                   \
	}                                                                                 \
	U##FUNCTION(BlueprintCallable) static void SetComp##C(FEnttHandle Handle, C Comp) \
	{                                                                                 \
		if (Handle.registry())                                                        \
			Handle.emplace_or_replace<C>(Comp);                                       \
	}

#define XSTR(x) STR(x)
#define STR(x) #x

UCLASS()
class URegComponentsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable) static bool HasCompFCompModuleBase(FEnttHandle Handle) { if (!Handle) return false; return Handle.has<FCompModuleBase>(); } UFUNCTION(BlueprintCallable) static FCompModuleBase GetCompFCompModuleBase(FEnttHandle Handle) { return !!Handle && Handle.has<FCompModuleBase>() ? Handle.get<FCompModuleBase>() : FCompModuleBase(); } UFUNCTION(BlueprintCallable) static void SetCompFCompModuleBase(FEnttHandle Handle, FCompModuleBase Comp) { if (Handle.registry()) Handle.emplace_or_replace<FCompModuleBase>(Comp); }

	/*
	COMP_DEF(FCompShipModuleBase)

#pragma message (XSTR(COMP_DEF(FCompShipModuleBase)))
	 */

};
