#pragma once
#include "../../Common/EnttHandle.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HelpersModulesDesc.generated.h"

USTRUCT(BlueprintType)
struct FModuleDesc
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGuid Guid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Properties;
};

UCLASS()
class UHelpersModulesDescLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, meta = (HidePin = "ContextObject", DefaultToSelf = "ContextObject"))
	static FModuleDesc GetModuleDesc(UObject* ContextObject, FEnttHandle Handle);
};
