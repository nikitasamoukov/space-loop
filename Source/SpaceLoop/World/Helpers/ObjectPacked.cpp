#include "ObjectPacked.h"

#include "SpaceLoop/World/Components/ComponentsModulesBase.h"
#include <SpaceLoop/Subsystems/RegistrySubsystem.h>
#include <Engine/GameEngine.h>

FText UObjectPackedShipModuleLibrary::GetName(FObjectPacked const& ObjectPacked)
{
	auto CompShipModuleBase = ObjectPacked.Components[0].GetPtr<FCompModuleBase>();
	if (!CompShipModuleBase)
		return FText::FromString("Not module");

	return CompShipModuleBase->Name;
}

FText UObjectPackedShipModuleLibrary::GetDescription(FObjectPacked const& ObjectPacked)
{
	auto CompShipModuleBase = ObjectPacked.Components[0].GetPtr<FCompModuleBase>();
	if (!CompShipModuleBase)
		return FText::FromString("Not module");

	return CompShipModuleBase->Desc;
}

FText UObjectPackedShipModuleLibrary::GetProperties(UObject* ContextObject, FObjectPacked const& ObjectPacked)
{
	FRegistry Registry;

	FEnttHandle Handle(Registry, Registry.create());

	auto World = ContextObject->GetWorld();
	auto RegistrySubsystem = World->GetGameInstance()->GetSubsystem<URegistrySubsystem>();

	ObjectPacked.Unpack(ContextObject, Handle);

	for (auto& PackedComp : ObjectPacked.Components)
	{
		auto StructName = PackedComp.GetScriptStruct()->GetStructCPPName();

		auto Cell = GFactoryComponents.GetTypeCell(FName(StructName));

		if (!Cell)
		{
			LOG("CompUnknown:" + StructName);
			continue;
		}

		if (Cell->FuncModuleDesc)
		{
			return Cell->FuncModuleDesc(Handle);
		}
	}

	LOG("No desc gen");
	return FText();
}

void FObjectPacked::Unpack(UObject* ContextObject, FEnttHandle Handle) const
{
	Unpack(ContextObject, Components, Handle);
}

void FObjectPacked::Unpack(UObject* ContextObject, TArray<FInstancedStruct> const& Components, FEnttHandle Handle)
{
	auto World = ContextObject->GetWorld();
	auto RegistrySubsystem = World->GetGameInstance()->GetSubsystem<URegistrySubsystem>();

	for (auto& PackedComp : Components)
	{
		if (!PackedComp.GetScriptStruct())
		{
			LOG("!PackedComp.GetScriptStruct(). Empty component in module?");
			continue;
		}

		auto StructName = PackedComp.GetScriptStruct()->GetStructCPPName();

		auto Cell = GFactoryComponents.GetTypeCell(FName(StructName));
		if (!Cell)
		{
			LOG("No cell for:" + StructName);
			continue;
		}
		if (!Cell->FuncUnpack)
		{
			LOG("No cell FuncUnpack for:" + StructName);
			continue;
		}
		Cell->FuncUnpack(Handle, PackedComp);
	}
}

FObjectPacked FObjectPacked::Pack(FFactoryComponents& FactoryComponents, FEnttHandle Handle)
{
	FObjectPacked ObjectPacked;

	for (auto Pair : Handle.storage())
	{
		auto Cell = FactoryComponents.GetTypeCell(Pair.first);

		FInstancedStruct PackedComp;

		Cell->FuncPack(Handle, PackedComp);

		ObjectPacked.Components.Add(PackedComp);
	}

	return ObjectPacked;
}
