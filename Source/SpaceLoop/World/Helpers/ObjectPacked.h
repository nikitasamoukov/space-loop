#pragma once
#include <InstancedStruct.h>
#include "Kismet/BlueprintFunctionLibrary.h"
#include <SpaceLoop/Common/EnttHandle.h>
#include "ObjectPacked.generated.h"

struct FFactoryComponents;

USTRUCT(BlueprintType)
struct FObjectPacked
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FInstancedStruct> Components;

	void Unpack(UObject* ContextObject, FEnttHandle Handle) const;
	static void Unpack(UObject* ContextObject, TArray<FInstancedStruct> const& Components, FEnttHandle Handle);

	static FObjectPacked Pack(FFactoryComponents& FactoryComponents, FEnttHandle Handle);
};

UCLASS()
class UObjectPackedShipModuleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static FText GetName(FObjectPacked const& ObjectPacked);
	UFUNCTION(BlueprintCallable)
	static FText GetDescription(FObjectPacked const& ObjectPacked);
	UFUNCTION(BlueprintCallable, meta = (HidePin = "ContextObject", DefaultToSelf = "ContextObject"))
	static FText GetProperties(UObject* ContextObject, FObjectPacked const& ObjectPacked);
};
