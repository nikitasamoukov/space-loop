#include "HelpersModules.h"
#include "Engine/World.h"
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>
#include <SpaceLoop/World/Components\FactoryComponents.h>

#include "SpaceLoop/Subsystems/DataStorageSubsystem.h"
#include "SpaceLoop/World/Components/ComponentsModulesWeapons.h"

double UHelpersModulesLibrary::GetWeaponBulletSpeed(FEnttHandle const& Handle)
{
	auto RetInf = 1'000'000'000'000'000'000.0;
	if (!Handle.IsValid())
	{
		LOG_ERROR();
		return RetInf;
	}
	if (auto CompDescWeaponCannon = Handle.try_get<FCompDescWeaponCannon>())
	{
		return CompDescWeaponCannon->Speed;
	}
	if (auto CompDescWeaponCannon = Handle.try_get<FCompDescWeaponLaser>())
	{
		return RetInf;
	}
	LOG_ERROR();
	return RetInf;
}

FEnttHandle CreateShipModule(UObject* ContextObject, FName ModuleId, FGuid Guid)
{
	auto World = ContextObject->GetWorld();
	auto DataStorageSubsystem = World->GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();

	auto TemplateHandle = DataStorageSubsystem->Objects.Find(ModuleId);
	if (!TemplateHandle)
	{
		LOG_CRIT();
		return {};
	}

	FEnttHandle Handle = CopyEntity(*TemplateHandle);

	auto CompModuleBase = Handle.try_get<FCompModuleBase>();

	if (!CompModuleBase)
	{
		LOG_CRIT();
		return {};
	}

	CompModuleBase->Guid = Guid;
	CompModuleBase->BaseModuleId = ModuleId;

	return Handle;
}

void Refill(FEnttHandle Handle)
{
	if (!Handle.IsValid())
		return;

	for (auto&& [IdType, Data] : Handle.storage())
	{
		auto Cell = GFactoryComponents.GetTypeCell(IdType);
		if (!Cell)
		{
			LOG("error on:" + FString::FromInt(IdType));
			continue;
		}
		if (Cell->FuncModuleRefill)
			Cell->FuncModuleRefill(Handle);
	}
}