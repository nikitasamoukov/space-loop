#include "HelpersModulesDesc.h"
#include "Engine/World.h"
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>
#include <SpaceLoop/Subsystems/RegistrySubsystem.h>

FText GetName(FEnttHandle Handle)
{
	FCompModuleBase t;
	auto CompModuleBase = Handle.try_get<FCompModuleBase>();
	if (!CompModuleBase)
		return FText::FromString("Not module");

	return CompModuleBase->Name;
}

FGuid GetGuid(FEnttHandle Handle)
{
	FCompModuleBase t;
	auto CompModuleBase = Handle.try_get<FCompModuleBase>();
	if (!CompModuleBase)
		return {};

	return CompModuleBase->Guid;
}

FText GetDescription(FEnttHandle Handle)
{
	FCompModuleBase t;
	auto CompModuleBase = Handle.try_get<FCompModuleBase>();
	if (!CompModuleBase)
		return FText::FromString("Not module");

	return CompModuleBase->Desc;
}

FText GetProperties(UObject* ContextObject, FEnttHandle Handle)
{
	if (!Handle.IsValid())
	{
		return FText();
	}

	auto World = ContextObject->GetWorld();
	auto RegistrySubsystem = World->GetGameInstance()->GetSubsystem<URegistrySubsystem>();

	FText Res;

	for (auto Pair : Handle.storage())
	{
		auto CompIdType = Pair.first;
		//LOG("CompTry:" + FString::FromInt(CompIdType));

		auto Cell = GFactoryComponents.GetTypeCell(CompIdType);

		if (!Cell)
		{
			LOG("CompUnknown:" + FString::FromInt(CompIdType));
			continue;
		}

		if (Cell->FuncModuleDesc)
		{
			auto Text = Cell->FuncModuleDesc(Handle);
			if (Res.IsEmpty())
			{
				Res = Text;
			}
			else
			{
				Res = FText::Format(FText::FromString("{0}\n{1}"), Res, Text);
			}
		}
	}

	return Res;
}

FModuleDesc UHelpersModulesDescLibrary::GetModuleDesc(UObject* ContextObject, FEnttHandle Handle)
{
	FModuleDesc Res;

	Res.Name = ::GetName(Handle);
	Res.Guid = GetGuid(Handle);
	Res.Description = GetDescription(Handle);
	Res.Properties = GetProperties(ContextObject, Handle);

	return Res;
}
