#pragma once
#include "ResourceMaterial.generated.h"

UENUM(BlueprintType)
enum class EResourceMaterial : uint8
{
	None,

	H,
	C,
	O,

	Fe,
	Cu,
	Si,

	U238,
	W,
	Ag,

	Au,
	Pt,
	Ir,

	Pu,
	U235,
	Th
};
