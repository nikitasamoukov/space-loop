#include "SystemShipHitsProcess.h"
#include "Engine/World.h"
#include <SpaceLoop/World/Components/ComponentsShip.h>
#include <SpaceLoop/Actors/Spaceship.h>
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>

#include "SpaceLoop/World/Components/ComponentsModulesArmors.h"
#include "SpaceLoop/World/Components/ComponentsModulesShields.h"
#include "SpaceLoop/World/Components/ComponentsModulesWeapons.h"

struct FSystemShipHitsProcess
{
	UWorld* World;
	FRegistry& Registry;

	void Run(float DeltaTime);
};

void FSystemShipHitsProcess::Run(float DeltaTime)
{
	// Shield
	for (auto [e, CompParentShip, CompModuleBase, CompShieldNormal] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompShieldNormal>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		auto Spaceship = CompParentShip.Spaceship;
		auto& CompShip = Spaceship->HandleShip.get<FCompShip>();
		auto& Hits = CompShip.Hits;
		for (int i = Hits.Num() - 1; i >= 0; i--)
		{
			auto& Hit = Hits[i];
			if (Hit.bPeriodic)
			{
				auto Dmg = Hit.Dmg * DeltaTime;
				if (Dmg > CompShieldNormal.Shield)
				{
					if (Dmg == 0)
					{
						LOG_ERROR();
						break;
					}
					CompShieldNormal.Shield = 0;
					CompModuleBase.ActivationProgress = 0;
					Hits.SetNum(0);
					break;
				}
				else
				{
					CompShieldNormal.Shield -= Dmg;
					Hits.SetNum(i);
				}
			}
			else
			{
				if (Hit.Dmg > CompShieldNormal.Shield)
				{
					CompShieldNormal.Shield = 0;
					CompModuleBase.ActivationProgress = 0;
					Hits.SetNum(0);
					break;
				}
				else
				{
					CompShieldNormal.Shield -= Hit.Dmg;
					Hits.SetNum(i);
				}
			}
		}
	}

	// Armor
	for (auto [e, CompParentShip, CompModuleBase, CompArmorNormal, CompDescArmorNormal] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompArmorNormal, FCompDescArmorNormal>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		if (CompArmorNormal.Armor == 0)
			continue;

		auto Spaceship = CompParentShip.Spaceship;
		auto& CompShip = Spaceship->HandleShip.get<FCompShip>();
		auto& Hits = CompShip.Hits;
		for (int i = Hits.Num() - 1; i >= 0; i--)
		{
			auto& Hit = Hits[i];

			Hit.Dmg -= CompDescArmorNormal.ArmorBlock;
			Hit.Dmg = FMath::Max(0, Hit.Dmg);

			if (Hit.bPeriodic)
			{
				auto Dmg = Hit.Dmg * DeltaTime;
				if (Dmg > CompArmorNormal.Armor)
				{
					if (Dmg == 0)
					{
						LOG_ERROR();
						break;
					}
					Hit.Dmg *= CompArmorNormal.Armor / Dmg;
					CompArmorNormal.Armor = 0;
					break;
				}
				else
				{
					CompArmorNormal.Armor -= Dmg;
					Hits.SetNum(i);
				}
			}
			else
			{
				if (Hit.Dmg > CompArmorNormal.Armor)
				{
					Hit.Dmg -= CompArmorNormal.Armor;
					CompArmorNormal.Armor = 0;
					break;
				}
				else
				{
					CompArmorNormal.Armor -= Hit.Dmg;
					Hits.SetNum(i);
				}
			}
		}
	}

	// Hull
	for (auto [e, CompShip] :
		Registry.view<FCompShip>().each())
	{
		auto Spaceship = CompShip.Spaceship;

		auto& Hits = CompShip.Hits;

		for (int i = Hits.Num() - 1; i >= 0; i--)
		{
			auto& Hit = Hits[i];

			double Dmg = 0;

			if (Hit.bPeriodic)
			{
				Dmg = Hit.Dmg * DeltaTime;
			}
			else
			{
				Dmg = Hit.Dmg;
			}

			Spaceship->Hp -= Dmg;

			Hits.SetNum(i);
		}

		if (Spaceship->Hp <= 0)
		{
			Spaceship->bDead = true;
			LOG("Dead");
		}
	}
}

void SystemShipHitsProcess(UWorld* World, FRegistry& Registry, float DeltaTime)
{
	FSystemShipHitsProcess SystemShipHitsProcess{ World, Registry };
	SystemShipHitsProcess.Run(DeltaTime);
}