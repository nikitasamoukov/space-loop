#include "SystemShipHitsProcess.h"
#include "Engine/World.h"
#include <SpaceLoop/World/Components/ComponentsShip.h>
#include <SpaceLoop/Actors/Spaceship.h>
#include <SpaceLoop/Actors/Bullet.h>
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>

#include "SpaceLoop/World/Components/ComponentsModulesWeapons.h"

struct FSystemShipWeapons
{
	UWorld* World;
	FRegistry& Registry;

	void Run(float DeltaTime);

	void UpdateMainWeapon(float DeltaTime);
};

void FSystemShipWeapons::Run(float DeltaTime)
{
	UpdateMainWeapon(DeltaTime);
}
void FSystemShipWeapons::UpdateMainWeapon(float DeltaTime)
{
	for (auto [e, CompParentShip] :
		Registry.view<FCompParentShip>().each())
	{
		auto Spaceship = CompParentShip.Spaceship.Get();
		Spaceship->ShipMainWeaponSfx->SetVisibility(false);
	}

	for (auto [e, CompParentShip, CompModuleBase, CompWeaponBase, CompDescWeaponLaser] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompWeaponBase, FCompDescWeaponLaser>().each())
	{
		if (!CompWeaponBase.IsShooting)
			continue;
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;

		auto Spaceship = CompParentShip.Spaceship.Get();

		if (Spaceship->ShipMainWeaponSfx->GetAsset() != CompDescWeaponLaser.LaserBeamSFX)
			Spaceship->ShipMainWeaponSfx->SetAsset(CompDescWeaponLaser.LaserBeamSFX);
		Spaceship->ShipMainWeaponSfx->SetVisibility(true);

		auto Trs = Spaceship->ShipMainWeaponSfx->GetComponentTransform();
		auto TrsInv = Trs.Inverse();

		auto WeaponAimDir = Spaceship->GetWeaponAimDir();

		auto EndPoint = Trs.GetLocation() + WeaponAimDir * CompDescWeaponLaser.Range;

		FCollisionQueryParams TraceParams;
		TraceParams.AddIgnoredActor(Spaceship);

		FHitResult HitResult;
		bool bHit = Spaceship->GetWorld()->LineTraceSingleByChannel(HitResult, Trs.GetLocation(), EndPoint, ECC_PhysicsBody, TraceParams);

		FVector PosHitLocal;

		if (bHit)
		{
			PosHitLocal = TrsInv.TransformPosition(Trs.GetLocation() + HitResult.Distance * WeaponAimDir);

			auto SpaceshipEnemy = Cast<ASpaceship>(HitResult.GetActor());

			if (SpaceshipEnemy)
			{
				auto& CompShip = SpaceshipEnemy->HandleShip.get<FCompShip>();
				CompShip.Hits.Add(FShipHit{ CompDescWeaponLaser.Dps, true });
			}
		}
		else
		{
			PosHitLocal = TrsInv.TransformPosition(Trs.GetLocation() + CompDescWeaponLaser.Range * WeaponAimDir);
		}

		// LOG("Niag pos Pos to fit:" + Spaceship->InputShotWorldPos.ToString() + " upd:" + PosHitLocal.ToString());
		Spaceship->ShipMainWeaponSfx->SetNiagaraVariableVec3("Beam End", PosHitLocal);
	}

	for (auto [e, CompParentShip, CompModuleBase, CompWeaponBase, CompDescWeaponCannon, CompWeaponCannon] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompWeaponBase, FCompDescWeaponCannon, FCompWeaponCannon>().each())
	{
		auto Spaceship = CompParentShip.Spaceship.Get();

		CompWeaponCannon.Reload = FMath::Max(CompWeaponCannon.Reload, 0) - DeltaTime;

		if (!CompWeaponBase.IsShooting)
			continue;
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		while (CompWeaponCannon.Reload <= 0)
		{
			CompWeaponCannon.Reload += 1.0 / CompDescWeaponCannon.AttackSpeed;

			auto Trs = Spaceship->ShipMainWeaponSfx->GetComponentTransform();

			auto WeaponAimDir = Spaceship->GetWeaponAimDir();
			FQuat Rot = WeaponAimDir.Rotation().Quaternion();
			Trs.SetRotation(Rot);

			bool EnergyDecreaseRes = Spaceship->EnergyDecrease(CompDescWeaponCannon.EnergyShot);
			if (!EnergyDecreaseRes)
			{
				break;
			}

			FActorSpawnParameters SpawnInfo;

			auto Bullet = World->SpawnActorDeferred<ABullet>(CompDescWeaponCannon.BulletActor.Get(), Trs);
			if (!Bullet)
			{
				LOG_CRIT();
				return;
			}

			FBulletPayload BulletPayload;
			BulletPayload.Dmg = CompDescWeaponCannon.Dmg;
			BulletPayload.Range = CompDescWeaponCannon.Range;
			BulletPayload.Speed = CompDescWeaponCannon.Speed;
			BulletPayload.ShipParent = Spaceship;
			Bullet->Init(BulletPayload);

			Bullet->FinishSpawning(Trs);
		}
	}
}

void SystemShipWeapons(UWorld* World, FRegistry& Registry, float DeltaTime)
{
	FSystemShipWeapons SystemShipWeapons{ World, Registry };
	SystemShipWeapons.Run(DeltaTime);
}