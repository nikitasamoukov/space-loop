#include "SystemModulesUpdate.h"
#include "Engine/World.h"
#include <SpaceLoop/World/Components/ComponentsShip.h>
#include <SpaceLoop/Actors/Spaceship.h>
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>

#include "SpaceLoop/World/Components/ComponentsModulesEnergyGen.h"
#include <SpaceLoop/World/Components/ComponentsModulesShields.h>
#include <SpaceLoop/World/Components/ComponentsModulesArmors.h>

struct FSystemModulesUpdate
{
	UWorld* World;
	FRegistry& Registry;

	void Run(float DeltaTime);

	void UpdateEnergyGen();
	void UpdateEnergyConsume();
	void UpdateActivation(float DeltaTime);
	void UpdateShields(float DeltaTime);
	void UpdateArmors(float DeltaTime);
};

void SystemModulesUpdate(UWorld* World, FRegistry& Registry, float DeltaTime)
{
	FSystemModulesUpdate SystemShipStatsCalculate{ World, Registry };
	SystemShipStatsCalculate.Run(DeltaTime);
}

void FSystemModulesUpdate::Run(float DeltaTime)
{
	UpdateEnergyGen();
	UpdateEnergyConsume();
	UpdateActivation(DeltaTime);

	UpdateShields(DeltaTime);
	UpdateArmors(DeltaTime);
}
void FSystemModulesUpdate::UpdateEnergyGen()
{
	for (auto [e, CompParentShip, CompModuleBase, CompEnergyGen, CompDescEnergyGenFuel] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompEnergyGen, FCompDescEnergyGenFuel>().each())
	{
		CompEnergyGen.EnergyGen = CompDescEnergyGenFuel.EnergyGenAmount;
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			CompEnergyGen.EnergyGen = 0;
	}
}
void FSystemModulesUpdate::UpdateEnergyConsume()
{
	for (auto [e, CompEnergyConsume] :
		Registry.view<FCompEnergyConsume>().each())
	{
		CompEnergyConsume.EnergyConsume = 0;
	}
	for (auto [e, CompParentShip, CompModuleBase, CompEnergyConsume, CompDescEnergyConsumeConst] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompEnergyConsume, FCompDescEnergyConsumeConst>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		CompEnergyConsume.EnergyConsume += CompDescEnergyConsumeConst.EnergyConsumeConst;
	}
}

void FSystemModulesUpdate::UpdateActivation(float DeltaTime)
{
	for (auto [e, CompModuleBase, CompParentShip] : Registry.view<FCompModuleBase, FCompParentShip>().each())
	{
		switch (CompModuleBase.ActivationType)
		{
			case EShipModuleActivationType::AlwaysActive:
				CompModuleBase.ActivationProgress = 1;
				break;
			case EShipModuleActivationType::OnlyTime:
				if (CompModuleBase.bEnabled)
				{
					CompModuleBase.ActivationProgress += DeltaTime / CompModuleBase.ActivationTime;
				}
				else
				{
					CompModuleBase.ActivationProgress = 0;
				}
				break;
			case EShipModuleActivationType::OnFullEnergy:
				if (CompModuleBase.bEnabled)
				{
					if (CompModuleBase.ActivationProgress > 0 || (CompParentShip.Spaceship->Energy > 0 && CompParentShip.Spaceship->Energy >= CompParentShip.Spaceship->EnergyMax))
						CompModuleBase.ActivationProgress += DeltaTime / CompModuleBase.ActivationTime;
				}
				else
				{
					CompModuleBase.ActivationProgress = 0;
				}
				break;
			case EShipModuleActivationType::None:
				LOG_ERROR();
				break;
		}
	}
}

void FSystemModulesUpdate::UpdateShields(float DeltaTime)
{
	for (auto [e, CompShip] : Registry.view<FCompShip>().each())
	{
		auto Spaceship = CompShip.Spaceship;
		Spaceship->Sp = 0;
		Spaceship->SpMax = 0;
		Spaceship->ShieldGuid = {};
	}

	for (auto [e, CompParentShip, CompModuleBase, CompDescShieldNormal, CompShieldNormal] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompDescShieldNormal, FCompShieldNormal>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
		{
			CompShieldNormal.Shield = 0;
			continue;
		}
		auto Spaceship = CompParentShip.Spaceship;

		if (CompParentShip.Spaceship->ShieldGuid != FGuid())
		{
			Spaceship->PowerFailure();
		}

		CompShieldNormal.Shield += CompDescShieldNormal.ShieldRegen * DeltaTime;
		CompShieldNormal.Shield = FMath::Clamp(CompShieldNormal.Shield, 0, CompDescShieldNormal.ShieldMax);

		CompParentShip.Spaceship->ShieldGuid = CompModuleBase.Guid;
		Spaceship->Sp = CompShieldNormal.Shield;
		Spaceship->SpMax = CompDescShieldNormal.ShieldMax;
	}
}

void FSystemModulesUpdate::UpdateArmors(float DeltaTime)
{
	for (auto [e, CompShip] : Registry.view<FCompShip>().each())
	{
		auto Spaceship = CompShip.Spaceship;
		Spaceship->Ap = 0;
		Spaceship->ApMax = 0;
	}

	for (auto [e, CompParentShip, CompModuleBase, CompDescArmorNormal, CompArmorNormal] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompDescArmorNormal, FCompArmorNormal>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
		{
			LOG("Armor inactive?");
			continue;
		}
		auto Spaceship = CompParentShip.Spaceship;

		Spaceship->Ap = CompArmorNormal.Armor;
		Spaceship->ApMax = CompDescArmorNormal.ArmorMax;
	}
}