#include "SystemsRunner.h"

#include "DrawDebugHelpers.h"
#include "SystemModulesUpdate.h"
#include "SystemShipHitsProcess.h"
#include "SystemShipStatsCalculate.h"
#include "SystemShipWeapons.h"
#include "SpaceLoop/Subsystems/PlayerDataSubsystem.h"
#include "SpaceLoop/World/Galaxy/Galaxy.h"

void SystemsRun(UWorld* World, FRegistry& Registry, float DeltaTime)
{
	SystemShipStatsCalculateRun(World, Registry, DeltaTime);

	SystemModulesUpdate(World, Registry, DeltaTime);

	SystemShipWeapons(World, Registry, DeltaTime);

	SystemShipHitsProcess(World, Registry, DeltaTime);

	// auto PlayerDataSubsystem = World->GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();
	// for (auto& Pair : PlayerDataSubsystem->Galaxy->Grid)
	// {
	// 	for (auto& Star : Pair.Value->Stars)
	// 	{
	// 		FVector Pos;
	// 		Pos = Star.Pos;
	// 		DrawDebugPoint(World, Pos, 2, FColor::FromHex("#FFFF44"));
	// 	}
	// }
}