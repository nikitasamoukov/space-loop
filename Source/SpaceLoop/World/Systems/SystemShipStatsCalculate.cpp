#include "SystemShipStatsCalculate.h"
#include "Engine/World.h"
#include <SpaceLoop/World/Components/ComponentsShip.h>
#include <SpaceLoop/Actors/Spaceship.h>
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>

#include "Components/StaticMeshComponent.h"
#include "SpaceLoop/World/Components/ComponentsModulesEnergyGen.h"
#include "SpaceLoop/World/Components/ComponentsModulesThrusters.h"

struct FSystemShipStatsCalculate
{
	UWorld* World;
	FRegistry& Registry;

	void Run(float DeltaTime);

	void Cleanup();
	void ModulesProcess(float DeltaTime);
	void ShipFinalCalc();
};

void FSystemShipStatsCalculate::Run(float DeltaTime)
{
	Cleanup();

	ModulesProcess(DeltaTime);

	ShipFinalCalc();
}

void FSystemShipStatsCalculate::Cleanup()
{
	for (auto [e, CompShip] : Registry.view<FCompShip>().each())
	{
		CompShip.Spaceship->HpMax = CompShip.Spaceship->HullHp;
		CompShip.Spaceship->ApMax = 0;
		CompShip.Spaceship->SpMax = 0;
		CompShip.Spaceship->EnergyMax = 0;
		CompShip.Spaceship->EngineForce = 0;
	}
}

void FSystemShipStatsCalculate::ModulesProcess(float DeltaTime)
{
	for (auto [e, CompParentShip, CompModuleBase, CompDescEnergyCap] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompDescEnergyCap>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		CompParentShip.Spaceship->EnergyMax += CompDescEnergyCap.EnergyCap;
	}
	for (auto [e, CompParentShip, CompModuleBase, CompDescThruster] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompDescThruster>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		CompParentShip.Spaceship->EngineForce = CompDescThruster.Thrust;
	}
	for (auto [e, CompParentShip, CompModuleBase, CompEnergyGen] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompEnergyGen>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active)
			continue;
		CompParentShip.Spaceship->Energy += CompEnergyGen.EnergyGen * DeltaTime;
	}
	for (auto [e, CompParentShip, CompModuleBase, CompEnergyConsume] :
		Registry.view<FCompParentShip, FCompModuleBase, FCompEnergyConsume>().each())
	{
		if (CompModuleBase.GetCurrentState() != EShipModuleCurrentState::Active && CompModuleBase.GetCurrentState() != EShipModuleCurrentState::ActivationInProgress)
			continue;
		CompParentShip.Spaceship->Energy -= CompEnergyConsume.EnergyConsume * DeltaTime;
	}
}

void FSystemShipStatsCalculate::ShipFinalCalc()
{
	for (auto [e, CompShip] : Registry.view<FCompShip>().each())
	{
		auto Spaceship = CompShip.Spaceship;
		Spaceship->Volume = 0;
		Spaceship->Mass = 0;
		Spaceship->MassModules = 0;
		Spaceship->MassInventory = 0;

		Spaceship->Energy = FMath::Clamp(Spaceship->Energy, -1, Spaceship->EnergyMax);
		if (Spaceship->Energy < 0)
		{
			Spaceship->Energy = 0;
			Spaceship->PowerFailure();
		}

		for (auto& Slot : Spaceship->Modules)
		{
			FEnttHandle Handle = Slot.Handle;
			if (!Handle.IsValid())
				continue;

			auto CompShipModuleBase = Handle.try_get<FCompModuleBase>();
			if (!CompShipModuleBase)
				continue;
			Spaceship->Volume += CompShipModuleBase->Volume;
			Spaceship->MassModules += CompShipModuleBase->Mass;
		}

		Spaceship->Mass = Spaceship->MassModules + Spaceship->MassInventory + Spaceship->MassHull;
		Spaceship->ShipMesh->SetMassOverrideInKg(NAME_None, Spaceship->Mass, true);
	}
}

void SystemShipStatsCalculateRun(UWorld* World, FRegistry& Registry, float DeltaTime)
{
	FSystemShipStatsCalculate SystemShipStatsCalculate{ World, Registry };
	SystemShipStatsCalculate.Run(DeltaTime);
}