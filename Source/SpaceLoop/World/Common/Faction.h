﻿#pragma once

#include "Engine/Texture.h"
#include <Faction.generated.h>

class ASpaceship;
USTRUCT(BlueprintType)
struct FFactionShipInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Strength = 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<ASpaceship> ShipBp;
};

USTRUCT(BlueprintType)
struct FFaction
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FName Id;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture* Icon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Relations = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FFactionShipInfo> Ships;
};

static inline FName FactionPlayer = "player";
static inline FName FactionHumanUn = "human_un";
static inline FName FactionHumanEmpire = "human_empire";
static inline FName FactionPirate = "pirate";
static inline FName FactionAi = "ai";