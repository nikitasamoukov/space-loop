#include "Spaceship.h"

#include "Engine/World.h"
#include "SpaceLoop/World/Components/ComponentsModulesWarpDrive.h"

#include <Components/StaticMeshComponent.h>
#include <SpaceLoop/Subsystems/DataStorageSubsystem.h>
#include <SpaceLoop/Subsystems/RegistrySubsystem.h>
#include <SpaceLoop/World/Components/ComponentsModulesBase.h>
#include <SpaceLoop/Subsystems/PlayerDataSubsystem.h>
#include <SpaceLoop/World/Components/ComponentsShip.h>

#include "SpaceLoop/World/Components/ComponentsModulesWeapons.h"
#include <SpaceLoop/World/Systems/SystemsRunner.h>
#include <SpaceLoop/Common/Rg128.h>
#include <SpaceLoop/World/Helpers/HelpersModules.h>

constexpr double ForceMult = 100000;
constexpr double ForceMultMoving = 0.8;

ASpaceship::ASpaceship()
{
	ShipMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipMesh"));
	RootComponent = ShipMesh;

	ShipMainWeaponSfx = CreateDefaultSubobject<UNiagaraComponent>(TEXT("MainWeaponSfx"));
	ShipMainWeaponSfx->AttachToComponent(ShipMesh, FAttachmentTransformRules::KeepRelativeTransform);

	PrimaryActorTick.bCanEverTick = true;
	// PrimaryActorTick.bTickEvenWhenPaused = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
}

void ASpaceship::BeginPlay()
{
	Super::BeginPlay();

	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();
	auto RegistrySubsystem = GetGameInstance()->GetSubsystem<URegistrySubsystem>();

	Modules.SetNum(0);

	// Weapons always first for shooter code. ASpaceship::UpdateWeapons()
	for (int i = 0; i < ModulesWeaponsCount; i++)
	{
		Modules.Add(FShipModuleSlot{ EShipModuleType::Weapon });
	}

	for (int i = 0; i < ModulesSupportCount; i++)
	{
		Modules.Add(FShipModuleSlot{ EShipModuleType::Support });
	}

	Modules.Add(FShipModuleSlot{ EShipModuleType::Armor });
	Modules.Add(FShipModuleSlot{ EShipModuleType::Thruster });
	Modules.Add(FShipModuleSlot{ EShipModuleType::WarpDrive });

	FRegistry TmpReg;

	if (Guid == FGuid())
	{
		auto MapName = GetWorld()->GetMapName();
		auto ActorName = GetName();

		FRgSeed Seed;
		Seed.Add(MapName);
		Seed.Add(ActorName);
		Guid = Seed.Seed;
	}

	FRg128 Rg128({ Guid });

	for (int i = 0; i < ModulesInitInfo.Num(); i++)
	{
		auto& ModuleId = ModulesInitInfo[i];

		auto ObjectPacked = DataStorageSubsystem->Objects.Find(ModuleId);
		if (!ObjectPacked)
		{
			LOG("No object id:" + ModuleId.ToString());
			continue;
		}

		FEnttHandle Handle(TmpReg, TmpReg.create());
		CopyEntity(*ObjectPacked, Handle);

		if (auto CompModuleBase = Handle.try_get<FCompModuleBase>())
		{

			CompModuleBase->Guid = Rg128.GenGuid();
		}

		bool bSuccess = false;

		for (int r = 0; r < Modules.Num(); r++)
		{
			auto bRes = AddModule(Handle, r);
			if (bRes)
			{
				bSuccess = true;
				break;
			}
		}

		if (!bSuccess)
		{
			LOG_CRIT();
			Handle.destroy();
		}
	}

	SystemsRun(GetWorld(), TmpReg, 0);

	for (auto& Slot : Modules)
	{
		if (!Slot.Handle.IsValid())
			continue;
		FEnttHandle Handle(RegistrySubsystem->Registry, RegistrySubsystem->Registry.create());
		CopyEntity(Slot.Handle, Handle);
		Slot.Handle = Handle;
	}

	for (auto& Slot : Modules)
	{
		if (!Slot.Handle.IsValid())
			continue;
		Refill(Slot.Handle);
	}

	HandleShip = RegistrySubsystem->CreateHandle();
	HandleShip.emplace<FCompShip>().Spaceship = this;

	EnableAllModules();
	FillAllStats();
}
void ASpaceship::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	for (auto& Module : Modules)
	{
		if (Module.Handle.IsValid())
		{
			Module.Handle.destroy();
			Module.Handle = {};
		}
	}
	HandleShip.destroy();
	HandleShip = {};
}

void ASpaceship::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TRACE_CPUPROFILER_EVENT_SCOPE_FUNCTION()

	Mass = FMath::Max(Mass, 1);

	// Inputs

	FVector2D InputRotate = {
		InputRotateForceZ, InputRotateForceY
	};

	if (InputRotate.Length() > 1)
	{
		InputRotate.Normalize();
	}

	if (bDead)
	{
		PowerFailure();
		Energy = 0;
	}

	InputRotateForceX = FMath::Clamp(InputRotateForceX, -1.0, 1.0);

	FVector InputForceVec(InputForceX, InputForceY, InputForceZ);
	if (InputForceVec.Length() > 1)
	{
		InputForceVec.Normalize();
	}
	InputForceX = InputForceVec.X;
	InputForceY = InputForceVec.Y;
	InputForceZ = InputForceVec.Z;

	{
		FQuat OriginRot = RootComponent->GetComponentTransform().GetRotation();
		FQuat RotQuat(OriginRot.GetAxisZ(), InputRotate.X);
		auto Mult = EngineForce * TorqueMult * ForceRotateZPercent / Mass;
		Mult = FMath::Min(Mult, ForceRotateSpeedLimit);
		Mult *= Mass;
		ShipMesh->AddTorqueInRadians(RotQuat.ToRotationVector() * Mult * ForceMult);
	}
	{
		FQuat OriginRot = RootComponent->GetComponentTransform().GetRotation();
		FQuat RotQuat(OriginRot.GetAxisY(), InputRotate.Y);
		auto Mult = EngineForce * TorqueMult * ForceRotateYPercent / Mass;
		Mult = FMath::Min(Mult, ForceRotateSpeedLimit);
		Mult *= Mass;
		ShipMesh->AddTorqueInRadians(RotQuat.ToRotationVector() * Mult * ForceMult);
	}
	{
		FQuat OriginRot = RootComponent->GetComponentTransform().GetRotation();
		FQuat RotQuat(OriginRot.GetAxisX(), -InputRotateForceX);
		auto Mult = EngineForce * TorqueMult * ForceRotateXPercent / Mass;
		Mult = FMath::Min(Mult, ForceRotateSpeedLimit);
		Mult *= Mass;
		ShipMesh->AddTorqueInRadians(RotQuat.ToRotationVector() * Mult * ForceMult);
	}

	{
		// Move
		FQuat OriginRot = RootComponent->GetComponentTransform().GetRotation();
		if (InputForceX > 0)
			ShipMesh->AddForce(OriginRot.GetAxisX() * InputForceX * EngineForce * ForceXpPercent * ForceMult * ForceMultMoving);
		else
			ShipMesh->AddForce(OriginRot.GetAxisX() * InputForceX * EngineForce * ForceXmPercent * ForceMult * ForceMultMoving);
		ShipMesh->AddForce(OriginRot.GetAxisY() * InputForceY * EngineForce * ForceYPercent * ForceMult * ForceMultMoving);
		ShipMesh->AddForce(OriginRot.GetAxisZ() * InputForceZ * EngineForce * ForceZPercent * ForceMult * ForceMultMoving);
	}
	UpdateWeapons();
}

void ASpaceship::FillAllStats()
{
	HpMax = HullHp;
	Hp = HpMax;
	Ap = ApMax;
	Energy = EnergyMax;
}

void ASpaceship::UpdateWeapons()
{
	if (!InputShotMainWeapon)
	{
		ShipMainWeaponSfx->SetVisibility(false);
	}

	for (int i = 0; i < ModulesWeaponsCount; i++)
	{
		auto H = Modules[i].Handle;
		if (!H.IsValid())
			continue;
		if (auto CompWeaponBase = H.try_get<FCompWeaponBase>())
		{
			CompWeaponBase->IsShooting = false;
		}
	}
	if (ModulesWeaponsCount == 0)
	{
		return;
	}
	SelectedWeapon = FMath::Clamp(SelectedWeapon, 0, ModulesWeaponsCount - 1);

	auto WeaponHandle = Modules[SelectedWeapon].Handle;

	if (!WeaponHandle.IsValid())
		return;

	auto CompWeaponBase = WeaponHandle.try_get<FCompWeaponBase>();
	if (!CompWeaponBase)
	{
		LOG_ERROR();
		return;
	}

	auto WeaponDir = ShipMainWeaponSfx->GetComponentTransform().GetRotation().GetAxisX();
	auto WeaponAimDir = GetWeaponAimDir();

	if (FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(WeaponDir, WeaponAimDir))) > 45)
	{
		CompWeaponBase->IsShooting = false;
	}
	else
	{
		CompWeaponBase->IsShooting = InputShotMainWeapon;
	}
}

bool ASpaceship::AddModule(FEnttHandle EnttHandle, int Idx)
{
	if (!Modules.IsValidIndex(Idx))
		return false;
	if (Modules[Idx].Handle.IsValid())
		return false;
	if (!UFShipModuleSlot::CanFitInSlot(Modules[Idx], EnttHandle))
		return false;

	EnttHandle.emplace<FCompParentShip>(this);

	Modules[Idx].Handle = EnttHandle;

	return true;
}

FVector ASpaceship::GetWeaponAimDir()
{
	auto Trs = ShipMainWeaponSfx->GetComponentTransform();
	auto WeaponAimDir = InputShotWorldPos - Trs.GetLocation();
	WeaponAimDir.Normalize();
	return WeaponAimDir;
}

void ASpaceship::EnableAllModules()
{
	for (auto& Slot : Modules)
	{
		if (!Slot.Handle.IsValid())
			continue;

		auto CompModuleBase = Slot.Handle.try_get<FCompModuleBase>();
		if (CompModuleBase)
			CompModuleBase->ActivationProgress = 1;
	}
}

bool ASpaceship::ExecuteItemDragAndDrop(FItemSlotIdx From, FItemSlotIdx To)
{
	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();

	auto IsValid = [&](FItemSlotIdx Slot) {
		switch (Slot.SlotType)
		{
			case EItemSlotType::Inventory:
				return PlayerDataSubsystem->Items.IsValidIndex(Slot.Idx);
			case EItemSlotType::ShipModules:
				return Modules.IsValidIndex(Slot.Idx);
			default:
				LOG_CRIT();
		}
		return false;
	};

	auto GetHandle = [&](FItemSlotIdx Slot) {
		switch (Slot.SlotType)
		{
			case EItemSlotType::Inventory:
				return PlayerDataSubsystem->Items[Slot.Idx];
			case EItemSlotType::ShipModules:
				return Modules[Slot.Idx].Handle;
			default:
				LOG_CRIT();
		}
		return FEnttHandle();
	};

	auto SetHandle = [&](FItemSlotIdx Slot, FEnttHandle Handle) {
		switch (Slot.SlotType)
		{
			case EItemSlotType::Inventory:
				PlayerDataSubsystem->Items[Slot.Idx] = Handle;
				break;
			case EItemSlotType::ShipModules:
				Modules[Slot.Idx].Handle = Handle;
				if (Handle.IsValid())
				{
					Handle.emplace<FCompParentShip>(FCompParentShip{ this });
					if (auto CompModuleBase = Handle.try_get<FCompModuleBase>())
					{
						CompModuleBase->ActivationProgress = 0;
					}
				}

				break;
			default:
				LOG_CRIT();
		}
	};

	auto CanFitInSlot = [&](FItemSlotIdx Slot, FEnttHandle Handle) {
		switch (Slot.SlotType)
		{
			case EItemSlotType::Inventory:
				return true;
			case EItemSlotType::ShipModules:
				return UFShipModuleSlot::CanFitInSlot(Modules[Slot.Idx], Handle);
			default:
				LOG_CRIT();
		}
		return false;
	};

	if (!IsValid(From) || !IsValid(To))
		return false;
	if (From == To)
		return false;

	auto H1 = GetHandle(From);
	auto H2 = GetHandle(To);

	if (!CanFitInSlot(From, H2) || !CanFitInSlot(To, H1))
		return false;

	if (H1.IsValid())
		(void)H1.remove<FCompParentShip>();
	if (H2.IsValid())
		(void)H2.remove<FCompParentShip>();

	SetHandle(From, H2);
	SetHandle(To, H1);

	return true;
}

void ASpaceship::PowerFailure()
{
	for (auto& Slot : Modules)
	{
		if (!Slot.Handle.IsValid())
			continue;

		auto CompModuleBase = Slot.Handle.try_get<FCompModuleBase>();
		if (!CompModuleBase)
		{
			LOG_ERROR();
			continue;
		}
		if (CompModuleBase->ActivationType != EShipModuleActivationType::AlwaysActive)
			CompModuleBase->ActivationProgress = 0;
	}
}
bool ASpaceship::EnergyDecrease(double EnergyWasted)
{
	Energy -= EnergyWasted;
	if (Energy < 0)
	{
		Energy = 0;
		PowerFailure();
		return false;
	}
	return true;
}
double ASpaceship::GetWarpDriveRange()
{
	if (Modules.Num() == 0)
	{
		LOG_ERROR();
		return 0;
	}
	auto& Slot = Modules[Modules.Num() - 1];
	if (!Slot.Handle.valid())
	{
		return 0;
	}
	auto CompDescWarpDrive = Slot.Handle.try_get<FCompDescWarpDrive>();
	if (!CompDescWarpDrive)
	{
		LOG_ERROR();
		return 0;
	}

	return CompDescWarpDrive->Range;
}

bool ASpaceship::TryExecJump(double Range)
{
	if (Modules.Num() == 0)
	{
		LOG_ERROR();
		return false;
	}
	auto& Slot = Modules[Modules.Num() - 1];
	if (!Slot.Handle.valid())
		return false;
	auto CompDescWarpDrive = Slot.Handle.try_get<FCompDescWarpDrive>();
	auto CompModuleBase = Slot.Handle.try_get<FCompModuleBase>();
	if (!CompDescWarpDrive || !CompModuleBase)
	{
		LOG_ERROR();
		return false;
	}

	if (CompModuleBase->GetCurrentState() != EShipModuleCurrentState::Active)
		return false;

	if (CompDescWarpDrive->Range < Range)
		return false;

	CompModuleBase->ActivationProgress = 0;

	return true;
}

FEnttHandle ASpaceship::GetCurrentWeapon()
{
	return Modules[SelectedWeapon].Handle;
}

TArray<FShipModuleSlotWithIdx> ASpaceship::GetSlotsByType(EShipModuleType SlotType)
{
	TArray<FShipModuleSlotWithIdx> Res;

	for (int i = 0; i < Modules.Num(); i++)
	{
		auto& Slot = Modules[i];
		if (Slot.SlotType == SlotType)
		{
			FShipModuleSlotWithIdx SlotIdx;
			SlotIdx.Handle = Slot.Handle;
			SlotIdx.SlotType = Slot.SlotType;
			SlotIdx.Idx = i;
			Res.Add(SlotIdx);
		}
	}
	return Res;
}
FText ASpaceship::GetShipDesc1()
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("Hp"), Hp);
	Args.Add(TEXT("HpMax"), HpMax);
	Args.Add(TEXT("Ap"), Ap);
	Args.Add(TEXT("ApMax"), ApMax);
	Args.Add(TEXT("Sp"), Sp);
	Args.Add(TEXT("SpMax"), SpMax);
	Args.Add(TEXT("Energy"), Energy);
	Args.Add(TEXT("EnergyMax"), EnergyMax);
	auto ForceMaxMult = FMath::Max(
		FMath::Max(ForceXpPercent, ForceXmPercent),
		FMath::Max(ForceYPercent, ForceZPercent));
	Args.Add(TEXT("EngineForceScaled"), EngineForce * ForceMaxMult);

	auto Text = FText::Format(FText::FromString(LR"ZZ(Хп: {Hp}/{HpMax}
Броня: {Ap}/{ApMax}
Щит: {Sp}/{SpMax}
Энергия: {Energy}/{EnergyMax}
Тяга(макс.): {EngineForceScaled})ZZ"),
		Args);

	return Text;
}
FText ASpaceship::GetShipDesc2()
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("Mass"), Mass);
	Args.Add(TEXT("MassHull"), MassHull);
	Args.Add(TEXT("MassModules"), MassModules);
	Args.Add(TEXT("MassInventory"), MassInventory);
	Args.Add(TEXT("Volume"), Volume);
	Args.Add(TEXT("VolumeMax"), HullVolume);

	auto Text = FText::Format(FText ::FromString(LR"ZZ(Масса: {Mass}({MassHull}+{MassModules}+{MassInventory})
Объём: {Volume}\{VolumeMax})ZZ"),
		Args);

	return Text;
}

void ASpaceship ::AddHitByShot(ABullet* Bullet)
{
	if (!Bullet)
	{
		LOG_CRIT();
		return;
	}
	auto& CompShip = HandleShip.get<FCompShip>();
	CompShip.Hits.Add(FShipHit{ Bullet->Payload.Dmg, false });
}