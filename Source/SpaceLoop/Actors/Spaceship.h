#pragma once

#include "CoreMinimal.h"
#include <SpaceLoop/Common/EnttHandle.h>
#include <SpaceLoop/UI/ItemSlotType.h>
#include <SpaceLoop/Structs/ModuleSlot.h>
#include "NiagaraComponent.h"
#include "SpaceLoop/World/Common/Faction.h"
#include "Bullet.h"
#include "Spaceship.generated.h"

UCLASS()
class SPACELOOP_API ASpaceship : public APawn
{
	GENERATED_BODY()

public:
	ASpaceship();

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* ShipMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UNiagaraComponent* ShipMainWeaponSfx = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticInitParam)
	TArray<FName> ModulesInitInfo;

	// StaticParam

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double HullVolume = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double MassHull = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double HullHp = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	int ModulesWeaponsCount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	int ModulesSupportCount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double TorqueMult = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceRotateXPercent = 1.0 / 6;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceRotateYPercent = 1.0 / 6;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceRotateZPercent = 1.0 / 6;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceRotateSpeedLimit = 800;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceXpPercent = 1.0 / 6;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceXmPercent = 1.0 / 6;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceYPercent = 1.0 / 6;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticParam)
	double ForceZPercent = 1.0 / 6;

	// Runtime

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	FEnttHandle HandleShip;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	TArray<FShipModuleSlot> Modules;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double Volume = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double Hp = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double HpMax = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double Ap = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double ApMax = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double Sp = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double SpMax = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	FGuid ShieldGuid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double Energy = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double EnergyMax = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double MassModules = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double MassInventory = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double Mass = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double MaxSpeed = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	double EngineForce = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	double InputRotateForceX = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	double InputRotateForceY = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	double InputRotateForceZ = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	double InputForceX = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	double InputForceY = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	double InputForceZ = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	bool InputShotMainWeapon = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	FVector InputShotWorldPos = FVector::Zero();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtime)
	int SelectedWeapon = 0;

	// Runtime not physycal things
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	FName FactionId = FactionPirate;

	// Ship guid. Used as seed. Auto generated if set to zero.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	FGuid Guid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Runtime)
	bool bDead = false;

	void FillAllStats();
	void UpdateWeapons();
	bool AddModule(FEnttHandle EnttHandle, int Idx);
	FVector GetWeaponAimDir();
	void PowerFailure();
	bool EnergyDecrease(double EnergyWasted);

	UFUNCTION(BlueprintCallable)
	double GetWarpDriveRange();

	UFUNCTION(BlueprintCallable)
	bool TryExecJump(double Range);

	UFUNCTION(BlueprintCallable)
	FEnttHandle GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void EnableAllModules();

	UFUNCTION(BlueprintCallable)
	bool ExecuteItemDragAndDrop(FItemSlotIdx From, FItemSlotIdx To);

	UFUNCTION(BlueprintCallable)
	TArray<FShipModuleSlotWithIdx> GetSlotsByType(EShipModuleType SlotType);

	UFUNCTION(BlueprintCallable)
	FText GetShipDesc1();

	UFUNCTION(BlueprintCallable)
	FText GetShipDesc2();

	UFUNCTION(BlueprintCallable)
	void AddHitByShot(ABullet* Bullet);
};