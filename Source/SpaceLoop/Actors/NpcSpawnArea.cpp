﻿#include "NpcSpawnArea.h"

ANpcSpawnArea::ANpcSpawnArea()
{
	auto Comp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Comp;
}

void ANpcSpawnArea::BeginPlay()
{
	Super::BeginPlay();
}

void ANpcSpawnArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}