﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpaceLoop/World/Galaxy/GalaxyStar.h"
#include "ExecutionDummy.generated.h"

class UGalaxyPreGeneratedDataAsset;
UCLASS()
class SPACELOOP_API AExecutionDummy : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Exec_Galaxy")
	UGalaxyPreGeneratedDataAsset* GalaxyPreGenerated;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Exec_Galaxy")
	FGuid ManualStarGuid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Exec_Galaxy")
	FGalaxyStar ManualStarData;

	UFUNCTION(CallInEditor, Category = "Exec_Galaxy")
	void GenerateGalaxy();

	UFUNCTION(CallInEditor, Category = "Exec_Galaxy")
	void ApplyManualStar();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Exec_Galaxy")
	FName FactionId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Exec_Galaxy")
	double FactionRange;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Exec_Galaxy")
	double FactionStrengthMax;

	UFUNCTION(CallInEditor, Category = "Exec_Galaxy")
	void ApplyFactionArea();
};