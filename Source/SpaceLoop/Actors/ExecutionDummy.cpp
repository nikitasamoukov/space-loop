﻿#include "ExecutionDummy.h"

#include "SpaceLoop/DataAssets/GalaxyPreGeneratedDataAsset.h"
#include "SpaceLoop/World/Galaxy/GalaxyGenerator.h"

void AExecutionDummy::GenerateGalaxy()
{
	if (!GalaxyPreGenerated)
	{
		LOG_ERROR();
		return;
	}

	FGalaxyGenerator Gen;

	Gen.Seed = FGuid("52345234-2324-2174-2324-235562324543");
	Gen.Generate();

	GalaxyPreGenerated->Stars = MoveTemp(Gen.GalaxyPreGenerated->Stars);
	GalaxyPreGenerated->Seed = Gen.GalaxyPreGenerated->Seed;
	GalaxyPreGenerated->StarsCount = GalaxyPreGenerated->Stars.Num();
	GalaxyPreGenerated->Modify();
}
void AExecutionDummy::ApplyManualStar()
{
	auto Star = GalaxyPreGenerated->Stars.FindByPredicate([&](FGalaxyStar const& StarE) {
		return StarE.Guid == ManualStarGuid;
	});
	if (!Star)
	{
		LOG_ERROR();
		return;
	}

	Star->FactionId = ManualStarData.FactionId;
	Star->FactionStrength = ManualStarData.FactionStrength;
	Star->LevelInfo = ManualStarData.LevelInfo;

	GalaxyPreGenerated->Modify();
}
void AExecutionDummy::ApplyFactionArea()
{
	if (!GalaxyPreGenerated)
	{
		LOG_ERROR();
		return;
	}

	auto StarCentre = GalaxyPreGenerated->Stars.FindByPredicate([&](FGalaxyStar const& StarE) {
		return StarE.Guid == ManualStarGuid;
	});

	if (!StarCentre)
	{
		LOG_ERROR();
		return;
	}

	for (auto& Star : GalaxyPreGenerated->Stars)
	{
		double Dist = FVector::Distance(StarCentre->Pos, Star.Pos);
		if (Dist > FactionRange)
			continue;

		double Perc = 1 - Dist / FactionRange;
		auto Str = exp(Perc * log(FactionStrengthMax));

		Star.FactionId = FactionId;
		Star.FactionStrength = Str;
	};

	GalaxyPreGenerated->Modify();
}