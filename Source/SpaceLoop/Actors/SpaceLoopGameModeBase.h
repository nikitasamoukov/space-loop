#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpaceLoopGameModeBase.generated.h"

class UGalaxyPreGeneratedDataAsset;
/**
 *
 */
UCLASS()
class SPACELOOP_API ASpaceLoopGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
public:
	
};