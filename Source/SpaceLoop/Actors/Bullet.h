#pragma once

#include "CoreMinimal.h"
#include <SpaceLoop/Common/EnttHandle.h>
#include <SpaceLoop/UI/ItemSlotType.h>
#include <SpaceLoop/Structs/ModuleSlot.h>
#include "NiagaraComponent.h"
#include "GameFramework/Pawn.h"
#include "SpaceLoop/World/Common/Faction.h"
#include "Bullet.generated.h"

USTRUCT(BlueprintType)
struct FBulletPayload
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Dmg = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Param)
	double Range = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Param)
	double Speed = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Param)
	AActor* ShipParent = nullptr;
};

UCLASS()
class SPACELOOP_API ABullet : public APawn
{
	GENERATED_BODY()

public:
	ABullet();

	void Init(FBulletPayload NewPayload);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Param)
	FBulletPayload Payload;
};
