#include "SpaceLoopGameModeBase.h"

#include "SpaceLoop/DataAssets/GalaxyPreGeneratedDataAsset.h"

#include <Engine/Engine.h>
#include <SpaceLoop/Subsystems/DataStorageSubsystem.h>

#include "SpaceLoop/Subsystems/PlayerDataSubsystem.h"
#include "SpaceLoop/World/Galaxy/Galaxy.h"

void ASpaceLoopGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	auto DataStorageSubsystem = GetGameInstance()->GetSubsystem<UDataStorageSubsystem>();
	auto PlayerDataSubsystem = GetGameInstance()->GetSubsystem<UPlayerDataSubsystem>();

	Super::InitGame(MapName, Options, ErrorMessage);

	DataStorageSubsystem->BeginPlay();
}