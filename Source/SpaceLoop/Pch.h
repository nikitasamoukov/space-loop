#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
// ReSharper disable once CppUnusedIncludeDirective
#include "Kismet/BlueprintFunctionLibrary.h"

#define ENTT_NO_ETO
#include <entt/entt.hpp>

DECLARE_LOG_CATEGORY_EXTERN(LogSpaceLoop, Log, All);

void LOG_IMPL(const char* File, int Line, const char* Function, FString const& Text);
void LOG_IMPLFatal(const char* File, int Line, const char* Function, FString const& Text);

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define LOG(msg) (LOG_IMPL(__FILENAME__, __LINE__, __FUNCTION__, (msg)))
#define LOG_FATAL(msg) (LOG_IMPL(__FILENAME__, __LINE__, __FUNCTION__, (msg)))
#define LOG_CRIT() (LOG_FATAL("CRITICAL ERROR"))
#define LOG_ERROR() (LOG_FATAL("CRITICAL ERROR"))

#define LOCTEXT_SL(InKey, InTextLiteral) NSLOCTEXT("SpaceLoop", InKey, InTextLiteral)

#define TRACE_CPUPROFILER_EVENT_SCOPE_FUNCTION() TRACE_CPUPROFILER_EVENT_SCOPE_STR(__FUNCTION__)
