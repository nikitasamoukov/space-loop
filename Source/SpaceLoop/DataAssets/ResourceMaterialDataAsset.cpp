#include "ResourceMaterialDataAsset.h"

FText UResourceMaterialDataAsset::GetShortName(EResourceMaterial ResourceMaterial)
{
	switch (ResourceMaterial)
	{
		case EResourceMaterial::U235:
		case EResourceMaterial::U238:
			return FText::FromString(TEXT("U"));
	}
	return UEnum::GetDisplayValueAsText(ResourceMaterial);
}

FText UResourceMaterialDataAsset::GetIzotopeNum(EResourceMaterial ResourceMaterial)
{
	switch (ResourceMaterial)
	{
		case EResourceMaterial::U235:
			return FText::FromString(TEXT("235"));
		case EResourceMaterial::U238:
			return FText::FromString(TEXT("238"));
	}
	return FText();
}
