#pragma once

#include "Engine/DataAsset.h"
#include "ResourceMaterialDataAsset.h"
#include "SpaceLoop/DataAssets/ObjectDataAsset.h"
#include "FactionDataAsset.h"
#include "SpaceLoopDataAsset.generated.h"

class UBaseLevelsDataAsset;
class UGalaxyPreGeneratedDataAsset;
class UGalaxyMapBpRefDataAsset;
class UGalaxyMapBpRef;
UCLASS(BlueprintType)
class USpaceLoopDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UResourcesMaterialsTableDataAsset* ResourcesMaterialsTableDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UObjectsArrayDataAsset* ObjectsArrayDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UFactionsTableDataAsset* FactionsTableDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UGalaxyMapBpRefDataAsset* GalaxyMapBpRef;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UGalaxyPreGeneratedDataAsset* GalaxyPreGenerated;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBaseLevelsDataAsset* BaseLevels;
};