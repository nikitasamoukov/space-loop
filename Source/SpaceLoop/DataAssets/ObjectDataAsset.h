#pragma once

#include "Engine/DataAsset.h"
#include "InstancedStruct.h"
#include "ObjectDataAsset.generated.h"

UCLASS(BlueprintType)
class UObjectDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Id;

	UPROPERTY(EditAnywhere)
	TArray<FInstancedStruct> Components;
};

UCLASS(BlueprintType)
class UObjectsArrayDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	TArray<UObjectDataAsset*> Objects;
};