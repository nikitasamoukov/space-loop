﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BaseLevelsDataAsset.generated.h"

UCLASS()
class SPACELOOP_API UBaseLevelsDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, TSoftObjectPtr<UWorld>> BaseLevels;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, TSoftObjectPtr<UWorld>> ManualLevels;
};