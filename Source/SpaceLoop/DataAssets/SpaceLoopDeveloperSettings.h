#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "SpaceLoopDataAsset.h"
#include "SpaceLoopDeveloperSettings.generated.h"

/**
 * 
 */
UCLASS(config = Game, defaultconfig)
class SPACELOOP_API USpaceLoopDeveloperSettings : public UDeveloperSettings
{
	GENERATED_BODY()
public:
	UPROPERTY(Config, EditAnywhere)
	TSoftObjectPtr<USpaceLoopDataAsset> SpaceLoopDataAsset;
};
