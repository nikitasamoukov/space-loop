﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SpaceLoop/World/Galaxy/GalaxyStar.h"
#include "GalaxyPreGeneratedDataAsset.generated.h"

UCLASS()
class SPACELOOP_API UGalaxyPreGeneratedDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGuid Seed;

	UPROPERTY()
	TArray<FGalaxyStar> Stars;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int StarsCount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGuid StartStar;	
};