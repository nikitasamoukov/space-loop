﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SpaceLoop/World/Common/Faction.h"
#include "UObject/Object.h"
#include "FactionDataAsset.generated.h"

UCLASS()
class SPACELOOP_API UFactionDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FFaction Faction;
};

UCLASS()
class SPACELOOP_API UFactionsTableDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, UFactionDataAsset*> Factions;
};