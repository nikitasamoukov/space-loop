#pragma once

#include "Engine/DataAsset.h"
#include <SpaceLoop/World/Resourses/ResourceMaterial.h>
#include "ResourceMaterialDataAsset.generated.h"

UCLASS(BlueprintType)
class UResourceMaterialDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Tier = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double BaseCost = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FLinearColor Color = { 0, 0, 0, 1 };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Desc;

	UFUNCTION(BlueprintPure, BlueprintCallable)
	static FText GetShortName(EResourceMaterial ResourceMaterial);
	UFUNCTION(BlueprintPure, BlueprintCallable)
	static FText GetIzotopeNum(EResourceMaterial ResourceMaterial);
};

UCLASS(BlueprintType)
class UResourcesMaterialsTableDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<EResourceMaterial, UResourceMaterialDataAsset*> Resources;
};
